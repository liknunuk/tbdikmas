<?php
class Model_pppaud
{
    private $table = "nama tabel";
    private $db;

    public function __construct()
    {
        $this->db = new Database();
    }


    public function setTransfer($data)
    {
        $tbpenerima = '';
        foreach ($data['penerima'] as $penerima) {
            $tbpenerima .= sprintf('%03d', $penerima) . ',';
        }
        $tbpenerima = rtrim($tbpenerima, ',');

        $sql = "INSERT INTO buktiTransfer SET bulan = :bulan , program = :program , tbIds = :tbpenerima";

        $this->db->query($sql);
        $this->db->bind('bulan', $data['bulan']);
        $this->db->bind('program', $data['program']);
        $this->db->bind('tbpenerima', $tbpenerima);
        $this->db->execute();
        return $this->db->rowCount();
    }

    public function buktiTransfer($bul, $pro = "%")
    {
        $sql = "SELECT * FROM buktiTransfer WHERE bulan = :bul && program LIKE :pro";
        $this->db->query($sql);
        $this->db->bind('bul', $bul);
        $this->db->bind('pro', $pro);
        return $this->db->resultSet();
    }

    public function penerimaBulanan($idxTrf)
    {
        $sql = "SELECT tbIds FROM buktiTransfer WHERE idxTrf = :idxTrf LIMIT 1";
        $this->db->query($sql);
        $this->db->bind('idxTrf', $idxTrf);
        $result = $this->db->resultOne();
        $penerima = $this->penerimaTransfer($result['tbIds']);
        return $penerima;
    }

    public function bulanTransfer($idxTrf)
    {
        $sql = "SELECT program,bulan FROM buktiTransfer WHERE idxTrf = :idxTrf LIMIT 1";
        $this->db->query($sql);
        $this->db->bind('idxTrf', $idxTrf);
        return $this->db->resultOne();
    }

    public function rekapJampel($bulan)
    {
        $sql = "SELECT * FROM viewJampel WHERE bulan =:bulan ORDER BY nomor";
        $this->db->query($sql);
        $this->db->bind('bulan', $bulan);
        return $this->db->resultSet();
    }

    private function penerimaTransfer($tbIds)
    {
        $tbs = explode(",", $tbIds);
        $tutors = [];
        foreach ($tbs as $tb) {
            $sql = "SELECT * FROM tutorBantu WHERE nomor = :tb LIMIT 1";
            $this->db->query($sql);
            $this->db->bind('tb', $tb);
            $res = $this->db->resultOne();
            array_push($tutors, $res);
        }
        return $tutors;
    }
}
