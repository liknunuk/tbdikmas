<?php
Class Model_informasi
{
    private $table = "tbInfo";
    private $db;
    // cols: idxInfo, author , waktu, judul, berita

    public function __construct()
    {
        $this->db = new Database();
    }

    public function infoTerkini(){
        $sql="SELECT idxInfo, author , waktu, judul, LEFT(berita,150) berita FROM " . $this->table . " ORDER BY waktu DESC LIMIT " . rows;
        $this->db->query($sql);
        return $this->db->resultSet();
    }

    public function setInfo($data){
        if($data['mod'] == 'baru'){
            $sql="INSERT INTO tbInfo SET author=:author , waktu=:waktu, judul=:judul, berita=:berita , idxInfo=:idxInfo";
        }else{
            $sql="UPDATE tbInfo SET author=:author , waktu=:waktu, judul=:judul, berita=:berita WHERE idxInfo=:idxInfo";
        }
            
        $this->db->query($sql);
        $this->db->bind('author',$data['author']);
        $this->db->bind('waktu',$data['waktu']);
        $this->db->bind('judul',$data['judul']);
        $this->db->bind('berita',$data['berita']);
        $this->db->bind('idxInfo',$data['idxInfo']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    public function detilInfo($idxInfo){
        $sql="SELECT * FROM tbInfo WHERE idxInfo=:idxInfo";
        $this->db->query($sql);
        $this->db->bind('idxInfo',$idxInfo);
        return $this->db->resultOne();
    }

}

/*
        $sql="";
        $this->db->query($sql);
        $this->db->bind();
        $this->db->execute();
        return $this->db->rowCount();
        return $this->db->resultOne();
        return $this->db->resultSet();
*/