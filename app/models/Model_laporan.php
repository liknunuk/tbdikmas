<?php
class Model_laporan
{
    private $table = "laporan";
    private $db;
    // cols: idxLaporan , tipelap , bulan , tbId , tanggal , dgLink

    public function __construct()
    {
        $this->db = new Database();
    }

    public function setLaporan($data)
    {
        $sql = "INSERT INTO laporan SET tipelap=:tipelap , bulan=:bulan , tbId=:tbId , tanggal=:tanggal , dgLink=:dgLink";
        $this->db->query($sql);
        $this->db->bind('tipelap', $data['tipelap']);
        $this->db->bind('bulan', $data['bulan']);
        $this->db->bind('tbId', $data['tbId']);
        $this->db->bind('tanggal', $data['tanggal']);
        $this->db->bind('dgLink', $data['dgLink']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    public function chgLaporan($data)
    {
        $sql = "UPDATE laporan SET tipelap=:tipelap , bulan=:bulan , tbId=:tbId , tanggal=:tanggal , dgLink=:dgLink WHERE idxLaporan = :idxLaporan";
        $this->db->query($sql);
        $this->db->bind('tipelap', $data['tipelap']);
        $this->db->bind('bulan', $data['bulan']);
        $this->db->bind('tbId', $data['tbId']);
        $this->db->bind('tanggal', $data['tanggal']);
        $this->db->bind('dgLink', $data['dgLink']);
        $this->db->bind('idxLaporan', $data['idxLaporan']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    public function rmvLaporan($id)
    {
        $sql = "DELETE FROM laporan WHERE idxLaporan=:id";
        $this->db->query($sql);
        $this->db->bind('id', $id);
        $this->db->execute();
        return $this->db->rowCount();
    }

    public function dftLaporan($tbId)
    {
        $sql = "SELECT * FROM laporan WHERE tbId=:tbId ORDER BY bulan DESC LIMIT " . rows;
        $this->db->query($sql);
        $this->db->bind('tbId', $tbId);
        return $this->db->resultSet();
    }

    public function laporanTB($id)
    {
        $sql = "SELECT * FROM laporan WHERE idxLaporan=:id LIMIT 1";
        $this->db->query($sql);
        $this->db->bind('id', $id);
        return $this->db->resultOne();
    }

    public function labul($bulan)
    {

        $sql = "SELECT idxLaporan , bulan , tanggal , tbId , ktb, namaPenerima , kota , dgLink FROM laporan, tutorBantu WHERE tutorBantu.nomor = tbId && bulan=:bulan ORDER BY tbId";
        $this->db->query($sql);
        $this->db->bind('bulan', $bulan);
        $this->db->execute();
        return $this->db->resultSet();
    }
}

/*
        $sql="";
        $this->db->query($sql);
        $this->db->bind();
        $this->db->execute();
        return $this->db->rowCount();
        return $this->db->resultOne();
        return $this->db->resultSet();
*/