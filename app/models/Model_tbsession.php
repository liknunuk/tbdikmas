<?php
class Model_tbsession
{
    private $table = "tbSession";
    private $db;

    public function __construct()
    {
        $this->db = new Database();
    }


    public function activeSession($tbid)
    {
        $endedSession = $this->stopSession($tbid);
        $sql = "SELECT sessionID, sessionName FROM " . $this->table . " WHERE sessioStatus ='Berjalan' && tbId=:tbid";
        $this->db->query($sql);
        $this->db->bind('tbid', $tbid);
        return $this->db->resultSet();
    }

    private function stopSession($tbid)
    {
        $saiki = date('Y-m-d');
        $sql = "UPDATE $this->table SET sessioStatus = 'Selesai' WHERE periodeEnded < :saiki && tbId=:tbid";
        $this->db->query($sql);
        $this->db->bind('saiki', $saiki);
        $this->db->bind('tbid', $tbid);
        $this->db->execute();
        return $this->db->rowCount();
    }

    public function sessionNumber($x = 0)
    {
        $sql = "SELECT COUNT(sessionID)+$x sessionID FROM tbSession";
        $this->db->query($sql);
        $sessionNumber = $this->db->resultOne();
        return $sessionNumber['sessionID'];
    }

    public function setSession($data)
    {

        $sql = "INSERT INTO " . $this->table . " SET sessionID = :sessionID , sessionName =:sessionName ,  tbId = :tbId , periodeBegin = :periodeBegin , periodeEnded = :periodeEnded , mjp=:mjp, sessioStatus = 'Berjalan' ";
        $this->db->query($sql);
        $this->db->bind('sessionID', $data['sessionID']);
        $this->db->bind('sessionName', $data['sessionName']);
        $this->db->bind('tbId', $data['tbid']);
        $this->db->bind('periodeBegin', $data['periodeBegin']);
        $this->db->bind('periodeEnded', $data['periodeEnded']);
        $this->db->bind('mjp', $data['mjp']);
        $this->db->execute();
        return $this->db->rowCount;
    }

    public function chgSession($data)
    {

        // print_r($data);
        // exit();
        $sql = "UPDATE " . $this->table . " SET sessionName =:sessionName , periodeBegin = :periodeBegin , periodeEnded = :periodeEnded , mjp=:mjp WHERE sessionID = :sessionID ";
        $this->db->query($sql);
        $this->db->bind('sessionName', $data['sessionName']);
        $this->db->bind('periodeBegin', $data['periodeBegin']);
        $this->db->bind('periodeEnded', $data['periodeEnded']);
        $this->db->bind('mjp', $data['mjp']);
        $this->db->bind('sessionID', $data['sessionID']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    public function sessionInfo($id)
    {
        $sql = "SELECT * FROM tbSession WHERE sessionID = :id";
        $this->db->query($sql);
        $this->db->bind('id', $id);
        return $this->db->resultOne();
    }

    public function removeRombel($data)
    {
        $sql = "UPDATE $this->table SET sessioStatus = 'Selesai' WHERE sessionID=:id LIMIT 1";
        $this->db->query($sql);
        $this->db->bind('id', $data['sessionID']);
        $this->db->execute();
        return $this->db->rowCount();
    }
}
