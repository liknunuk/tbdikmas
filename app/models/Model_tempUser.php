<?php
class Model_tempUser
{
    private $table = "tempUser";
    private $db;

    public function __construct()
    {
        $this->db = new Database();
    }

    public function randUsername(){
        $number = mt_rand(401,499);
        $indeks = mt_rand(0,13);
        $string = ['amungme','devayan','fordata','haloban','korowai','lampung','makasar','morotai','sigulai','singkil','sumbawa','tamiang','tengger','ternate'];
        $username = $string[$indeks].$number;
        return $username;
    }

    public function randPassword(){
        $number = mt_rand(801,899);
        $indeks = mt_rand(0,16);
        $string = ['aries','canes','canis','canis','cetus','draco','hydra','indus','lepus','libra','lupus','mensa','musca','norma','orion','pyxis','virgo'];
        $password = $string[$indeks].$number;
        return $password;
    }

    public function randKtb(){
        $pref = ['TO','ET','OR','BA','NT','OE'];
        $x = mt_rand(0,5);
        $sql="SELECT COUNT(ktb) ktb FROM tempUser";
        $this->db->query($sql);
        $res = $this->db->resultOne();
        $nus =  $res['ktb'] + 203;
        $ktb = $pref[$x] . $nus;
        return $ktb;
    }

    public function setUser($data){

        $sql = "INSERT INTO tempUser SET ktb=:ktb , program=:program , namaPenerima=:namaPenerima , kodeNPWP=:kodeNPWP , namaBank=:namaBank , nomorRekening=:nomorRekening , namaRekTab=:namaRekTab , kota=:kota , tbUsername=:tbUsername , tbPassword=:tbPassword";
        $this->db->query($sql);
        $this->db->bind('ktb',$data['ktb']);
        $this->db->bind('program',$data['program']);
        $this->db->bind('namaPenerima',$data['namaPenerima']);
        $this->db->bind('kodeNPWP',$data['kodeNPWP']);
        $this->db->bind('namaBank',$data['namaBank']);
        $this->db->bind('nomorRekening',$data['nomorRekening']);
        $this->db->bind('namaRekTab',$data['namaRekTab']);
        $this->db->bind('kota',$data['kota']);
        $this->db->bind('tbUsername',$data['tbUsername']);
        $this->db->bind('tbPassword',$data['tbPassword']);
        $this->db->execute();

        return $this->db->rowCount();
    }
    

}
