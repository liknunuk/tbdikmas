<?php
class Model_presensi
{
    private $table = "jurnalKegiatan";
    private $db;
    // cols: idxJurnal , sessionID , tanggal, jam, kegiatan

    public function __construct()
    {
        $this->db = new Database();
    }

    public function pesertaSession($kelas)
    {
        $sql = "SELECT idxTadik , namaLengkap FROM pesertaDidik WHERE sessionID=:kelas ORDER BY namaLengkap";
        $this->db->query($sql);
        $this->db->bind('kelas', $kelas);
        return $this->db->resultSet();
    }

    public function setPresensi($data)
    {
        $sql = "INSERT INTO presensi SET sessionID=:sessionID , idxJurnal=:idxJurnal , hadirin=:hadirin";
        $this->db->query($sql);
        $this->db->bind('sessionID', $data['sessionID']);
        $this->db->bind('idxJurnal', $data['idxJurnal']);
        $this->db->bind('hadirin', rtrim($data['hadirin'], ","));
        $this->db->execute();
        return $this->db->rowCount();
    }

    public function dftPresensi($sessionID)
    {

        $sql = "SELECT presensi.idxPresensi , presensi.sessionID , presensi.idxJurnal , jurnalKegiatan.kegiatan , jurnalKegiatan.tanggal , jurnalKegiatan.jamMulai , jurnalKegiatan.jamAkhir FROM presensi , jurnalKegiatan WHERE jurnalKegiatan.idxJurnal = presensi.idxJurnal && presensi.sessionID=:sessionID ORDER BY tanggal DESC LIMIT " . rows;

        $this->db->query($sql);
        $this->db->bind('sessionID', $sessionID);
        return $this->db->resultSet();
    }

    public function hadirin($id)
    {
        $sql = "SELECT hadirin FROM presensi WHERE idxPresensi=:id";
        $this->db->query($sql);
        $this->db->bind('id', $id);
        $this->db->execute();
        $presensi = $this->db->resultOne();
        $idxTadik = explode(',', $presensi['hadirin']);
        $hadirin = [];
        foreach ($idxTadik as $tadik) {
            $pd = $this->dtlTadik($tadik);
            array_push($hadirin, $pd);
        }
        return $hadirin;
    }

    public function rasidoteko($data)
    {
        $sql = "DELETE FROM presensi WHERE idxPresensi=:idxPresensi LIMIT 1";
        $this->db->query($sql);
        $this->db->bind('idxPresensi', $data['idxPresensi']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    private function dtlTadik($idxTadik)
    {
        // $row = ( $pn -1 ) * rows;
        $sql = "SELECT  * FROM pesertaDidik WHERE idxTadik = :idxTadik ORDER BY idxTadik LIMIT 1";
        $this->db->query($sql);
        $this->db->bind('idxTadik', $idxTadik);
        return $this->db->resultOne();
    }
}

/*
        $sql="";
        $this->db->query($sql);
        $this->db->bind();
        $this->db->execute();
        return $this->db->rowCount();
        return $this->db->resultOne();
        return $this->db->resultSet();
*/