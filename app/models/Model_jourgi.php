<?php
class Model_jourgi
{
    private $table = "jurnalKegiatan";
    private $db;
    // cols: idxJurnal , sessionID , tanggal, jam, kegiatan

    public function __construct()
    {
        $this->db = new Database();
    }

    public function nambahJurnal($data)
    {
        $sql = "INSERT INTO " . $this->table . " SET sessionID=:sessionID, tanggal=:tanggal , jamMulai=:jamMulai , jamAkhir=:jamAkhir , kegiatan=:kegiatan";

        $this->db->query($sql);
        $this->db->bind('sessionID', $data['sessionID']);
        $this->db->bind('tanggal', $data['tanggal']);
        $this->db->bind('jamMulai', $data['jamMulai']);
        $this->db->bind('jamAkhir', $data['jamAkhir']);
        $this->db->bind('kegiatan', $data['kegiatan']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    public function ngubahJurnal($data)
    {
        $sql = "UPDATE " . $this->table . " SET sessionID=:sessionID, tanggal=:tanggal , jamMulai=:jamMulai , jamAkhir=:jamAkhir , kegiatan=:kegiatan WHERE idxJurnal=:idxJurnal";

        $this->db->query($sql);
        $this->db->bind('sessionID', $data['sessionID']);
        $this->db->bind('tanggal', $data['tanggal']);
        $this->db->bind('jamMulai', $data['jamMulai']);
        $this->db->bind('jamAkhir', $data['jamAkhir']);
        $this->db->bind('kegiatan', $data['kegiatan']);
        $this->db->bind('idxJurnal', $data['idxJurnal']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    public function mbuangJurnal($idxJurnal)
    {
        $sql = "DELETE FROM " . $this->table . " WHERE idxJurnal=:idxJurnal LIMIT 1";
        $this->db->query($sql);
        $this->db->bind('idxJurnal', $idxJurnal);
        $this->db->execute();
        return $this->db->rowCount();
    }

    public function daftarJurnal($sessionID, $pn = 1)
    {
        $row = ($pn - 1) * rows;

        $sql = "SELECT idxJurnal , sessionID , tanggal tgurut , DATE_FORMAT( tanggal,'%d - %m - %Y' ) tanggal, jamMulai, jamAkhir, CONVERT ( jamAkhir - jamMulai , TIME ) jampel ,kegiatan FROM " . $this->table . " WHERE sessionID=:sessionID ORDER BY tgurut  DESC , jamAkhir DESC LIMIT $row , " . rows;
        $this->db->query($sql);
        $this->db->bind('sessionID', $sessionID);
        return $this->db->resultSet();
    }


    public function detailJurnal($idxJurnal)
    {
        $sql = "SELECT jurnalKegiatan.* , tbSession.sessionName FROM " . $this->table . " , tbSession WHERE idxJurnal=:idxJurnal && tbSession.sessionID = jurnalKegiatan.sessionID";
        $this->db->query($sql);
        $this->db->bind('idxJurnal', $idxJurnal);
        return $this->db->resultOne();
    }

    public function jpjurnal($tbId, $bulan)
    {
        $sql = "SELECT jk.idxJurnal , ts.sessionName sessionName , ts.mjp, jk.tanggal , jk.kegiatan , jk.jamMulai , jk.jamAkhir ,CONVERT( (jk.jamAkhir - jk.jamMulai), TIME  ) durasi FROM tutorBantu tb, tbSession ts , jurnalKegiatan jk WHERE ts.sessioStatus = 'Berjalan' && ts.sessionID = jk.SessionID && tb.nomor = ts.tbID && jk.tanggal LIKE :bulan && tb.nomor = :tbId ORDER BY ts.sessionName , jk.tanggal, jk.jamMulai";

        $this->db->query($sql);
        $this->db->bind('bulan', $bulan . "%");
        $this->db->bind('tbId', $tbId);
        return $this->db->resultSet();
    }

    public function masaJurnalAktif($tbid)
    {
        $sql = "SELECT LEFT(jk.tanggal,7) bulan , jk.sessionID , ts.tbId FROM jurnalKegiatan jk , tbSession ts WHERE ts.tbId = :tbid && jk.sessionID = ts.sessionID && ts.sessioStatus='Berjalan' GROUP BY LEFT(jk.tanggal,7)";
        $this->db->query($sql);
        $this->db->bind('tbid', $tbid);
        return $this->db->resultSet();
    }

    public function rekapJpJurnal($bulan)
    {
        $sql = "SELECT nomor , namaPenerima , FLOOR(SUM(durasi/CAST(mjp AS signed))) jampel FROM viewJpJurnal WHERE left(tanggal,7) = :bulan GROUP BY  nomor ORDER BY nomor";
        $this->db->query($sql);
        $this->db->bind('bulan', $bulan);
        return $this->db->resultSet();
    }

    public function tutorJpJurnal($nomor, $bulan)
    {

        $sql = "SELECT FLOOR(SUM(durasi/CAST(mjp AS signed))) jampel FROM viewJpJurnal WHERE left(tanggal,7) = :bulan  && nomor=:nomor";
        $this->db->query($sql);
        $this->db->bind('bulan', $bulan);
        $this->db->bind('nomor', $nomor);
        return $this->db->resultOne();
    }
}
