<?php
class Model_tutors
{
    private $table = "nama tabel";
    private $db;

    public function __construct()
    {
        $this->db = new Database();
    }


    public function daftarTutor($pn = 1)
    {
        $row = ($pn - 1) * rows;
        $sql = "SELECT nomor, ktb , program, namaPenerima, ktb , program , namaPenerima , namaRekTab , kodeNPWP , namaBank , nomorRekening , nilaiRupiah , kota FROM tutorBantu ORDER BY nomor LIMIT $row," . rows;
        $this->db->query($sql);
        return $this->db->resultSet();
    }

    public function tutorByProgram($program)
    {
        $sql = "SELECT nomor , namaPenerima , kota FROM tutorBantu WHERE program = :program ORDER BY kota, namaPenerima";
        $this->db->query($sql);
        $this->db->bind('program', $program);
        return $this->db->resultSet();
    }

    public function tutorByGroup($group = 'TP')
    {
        $sql = "SELECT nomor, ktb , namaPenerima , kota FROM tutorBantu WHERE ktb LIKE :group ORDER BY ktb";
        $this->db->query($sql);
        $this->db->bind('group', $group . '%');
        return $this->db->resultSet();
    }

    public function dataTutor($id)
    {
        $sql = "SELECT nomor,ktb,program,namaPenerima,kodeNPWP,namaRekTab,namaBank,nomorRekening,nilaiRupiah,kota,tbUsername sesulih FROM tutorBantu WHERE nomor = :id";

        $this->db->query($sql);
        $this->db->bind('id', $id);
        return $this->db->resultOne();
    }

    public function myktb($nomor)
    {
        $sql = "SELECT ktb FROM tutorBantu WHERE nomor=:nomor";
        $this->db->query($sql);
        $this->db->bind('nomor', $nomor);
        $result = $this->db->resultOne();
        return $result['ktb'];
    }

    public function tutorBaru($data)
    {
        $tbPassword = md5($data['tbUsername'] . '++' . $data['tbPassword']);
        $sql = "INSERT INTO tutorBantu SET program = :program , namaPenerima = :namaPenerima , kodeNPWP = :kodeNPWP , kota = :Kota , namaRekTab = :namaRekTab ,namaBank = :namaBank , nomorRekening = :nomorRekening , nilaiRupiah = :nilaiRupiah,tbUsername=:tbUsername,tbPassword=:tbPassword";

        $this->db->query($sql);
        $this->db->bind('program', $data['program']);
        $this->db->bind('namaPenerima', $data['namaPenerima']);
        $this->db->bind('kodeNPWP', $data['kodeNPWP']);
        $this->db->bind('Kota', $data['Kota']);
        $this->db->bind('namaRekTab', $data['namaRekTab']);
        $this->db->bind('namaBank', $data['namaBank']);
        $this->db->bind('nomorRekening', $data['nomorRekening']);
        $this->db->bind('nilaiRupiah', $data['nilaiRupiah']);
        $this->db->bind('tbUsername', $data['tbUsername']);
        $this->db->bind('tbPassword', $tbPassword);

        $this->db->execute();
        return $this->db->rowCount();
    }

    public function tutorLama($data)
    {

        $sql = "UPDATE tutorBantu SET program = :program , namaPenerima = :namaPenerima , kodeNPWP = :kodeNPWP , kota = :Kota , namaRekTab = :namaRekTab ,namaBank = :namaBank , nomorRekening = :nomorRekening , nilaiRupiah = :nilaiRupiah WHERE nomor = :nomor ";

        $this->db->query($sql);
        $this->db->bind('program', $data['program']);
        $this->db->bind('namaPenerima', $data['namaPenerima']);
        $this->db->bind('kodeNPWP', $data['kodeNPWP']);
        $this->db->bind('Kota', $data['Kota']);
        $this->db->bind('namaRekTab', $data['namaRekTab']);
        $this->db->bind('namaBank', $data['namaBank']);
        $this->db->bind('nomorRekening', $data['nomorRekening']);
        $this->db->bind('nilaiRupiah', $data['nilaiRupiah']);
        $this->db->bind('nomor', $data['nomor']);

        $this->db->execute();
        return $this->db->rowCount();
    }

    public function tutorUbah($data)
    {

        $sql = "UPDATE tutorBantu SET program = :program , namaPenerima = :namaPenerima , kodeNPWP = :kodeNPWP , kota = :kota , nomorRekening = :nomorRekening WHERE nomor = :nomor ";

        $this->db->query($sql);
        $this->db->bind('program', $data['program']);
        $this->db->bind('namaPenerima', $data['namaPenerima']);
        $this->db->bind('kodeNPWP', $data['kodeNPWP']);
        $this->db->bind('kota', $data['kota']);
        $this->db->bind('nomorRekening', $data['nomorRekening']);
        $this->db->bind('nomor', $data['nomor']);

        $this->db->execute();
        return $this->db->rowCount();
    }

    public function tbfired($id)
    {
        $sql = "DELETE FROM tutorBantu WHERE NOMOR = :id LIMIT 1";
        $this->db->query($sql);
        $this->db->bind('id', $id);
        $this->db->execute();
        return $this->db->rowCount();
    }

    public function tbauth($data)
    {
        $password = md5(strtolower($data['username']) . '++' . $data['password']);
        $sql = "SELECT nomor, namaPenerima, program, kota FROM tutorBantu WHERE tbPassword=:password";
        $this->db->query($sql);
        $this->db->bind('password', $password);
        return $this->db->resultOne();
    }

    public function dfTrx($id)
    {
        $id = sprintf('%03d', $id);
        $sql = "SELECT DATE_FORMAT(waktu,'%d - %m - %Y %H:%i:%s')waktu , bulan FROM buktiTransfer WHERE tbIds LIKE :id ";
        $this->db->query($sql);
        $this->db->bind('id', '%' . $id . '%');
        return $this->db->resultSet();
    }

    public function matchTB($data)
    {
        $sql = "SELECT program, namaPenerima, kota, ktb FROM tutorBantu WHERE kodeNPWP = :kodeNPWP && nomorRekening = :nomorRekening LIMIT 1";
        $this->db->query($sql);
        $this->db->bind('kodeNPWP', $data['kodeNPWP']);
        $this->db->bind('nomorRekening', $data['nomorRekening']);

        return $this->db->resultOne();
    }

    public function credReset($data)
    {
        $tbPassword = md5($data['usr'] . '++' . $data['psw']);
        $sql = "UPDATE tutorBantu SET tbUsername=:usr, tbPassword=:tbPassword WHERE ktb=:ktb";
        $this->db->query($sql);
        $this->db->bind('usr', $data['usr']);
        $this->db->bind('tbPassword', $tbPassword);
        $this->db->bind('ktb', $data['ktb']);

        $this->db->execute();
        return $this->db->rowCount();
    }

    public function tutorByKtb($ktb)
    {
        $sql = "SELECT program, namaPenerima, kota FROM tutorBantu WHERE ktb=:ktb ";
        $this->db->query($sql);
        $this->db->bind('ktb', $ktb);
        return $this->db->resultOne();
    }

    public function tutorByNames($name)
    {
        $sql = "SELECT nomor, ktb , program, namaPenerima, ktb , program , namaPenerima , namaRekTab , kodeNPWP , namaBank , nomorRekening , nilaiRupiah , kota FROM tutorBantu WHERE namaPenerima LIKE :name";
        $this->db->query($sql);
        $this->db->bind('name', "%$name%");
        return $this->db->resultSet();
    }

    public function allTutor()
    {
        $sql = "SELECT nomor , namaPenerima , kota FROM tutorBantu ORDER BY nomor";
        $this->db->query($sql);
        return $this->db->resultSet();
    }

    public function setorJP($data)
    {
        $sql = "INSERT INTO rekapJampel SET ktb=:ktb, bulan=:bulan , jampel=:jampel ";

        $this->db->query($sql);
        $this->db->bind('ktb', $data['ktb']);
        $this->db->bind('bulan', $data['bulan']);
        $this->db->bind('jampel', $data['jampel']);

        $this->db->execute();
        return $this->db->rowCount();
    }

    // tabel tb temporer (tempTB)

    public function kotatb()
    {
        $sql = "SELECT DISTINCT(kota) kota FROM tutorBantu ORDER BY kota";
        $this->db->query($sql);
        return $this->db->resultSet();
    }

    public function tbKota($kota)
    {
        $sql = "SELECT * FROM tutorBantu WHERE kota =:kota";
        $this->db->query($sql);
        $this->db->bind('kota', $kota);
        return $this->db->resultSet();
    }

    public function bosenlogin($data)
    {
        $sql = "UPDATE tutorBantu SET tbUsername=:tbUsername, tbPassword=:tbPassword WHERE ktb=:ktb LIMIT 1";
        $this->db->query($sql);
        $this->db->bind('tbUsername', $data['tbUsername']);
        $this->db->bind('tbPassword', $data['tbPassword']);
        $this->db->bind('ktb', $data['ktb']);
        $this->db->execute();
        return $this->db->rowCount();
    }
}
