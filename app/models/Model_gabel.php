<?php
class Model_gabel
{
    private $table = "pesertaDidik";
    // cols: sessionID,idxTadik,namaLengkap,gender,tempatLahir,tanggalLahir
    private $db;

    public function __construct()
    {
        $this->db = new Database();
    }

    public function urutBaru($kelastb){
        $sql = "SELECT MAX(RIGHT(idxTadik,2) + 1) urut FROM " . $this->table . " WHERE idxTadik LIKE :kelastb";
        $this->db->query($sql);
        $this->db->bind('kelastb',$kelastb."%");
        $result = $this->db->resultOne();
        
        $urut = $result['urut'] == NULL ? '01' : sprintf('%02d',$result['urut']);
        return $urut;
    }

    public function newTadik($data){
        $sql = " INSERT INTO pesertaDidik SET sessionID = :sessionID , idxTadik = :idxTadik , namaLengkap = :namaLengkap , gender = :gender , tempatLahir = :tempatLahir , tanggalLahir = :tanggalLahir "; 

        $this->db->query($sql);
        $this->db->bind('sessionID',$data['sessionID']);
        $this->db->bind('idxTadik',$data['idxTadik']);
        $this->db->bind('namaLengkap',$data['namaLengkap']);
        $this->db->bind('gender',$data['gender']);
        $this->db->bind('tempatLahir',$data['tempatLahir']);
        $this->db->bind('tanggalLahir',$data['tanggalLahir']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    public function chgTadik($data){
        $sql = " UPDATE pesertaDidik SET sessionID = :sessionID , namaLengkap = :namaLengkap , gender = :gender , tempatLahir = :tempatLahir , tanggalLahir = :tanggalLahir WHERE idxTadik = :idxTadik "; 

        $this->db->query($sql);
        $this->db->bind('sessionID',$data['sessionID']);
        $this->db->bind('idxTadik',$data['idxTadik']);
        $this->db->bind('namaLengkap',$data['namaLengkap']);
        $this->db->bind('gender',$data['gender']);
        $this->db->bind('tempatLahir',$data['tempatLahir']);
        $this->db->bind('tanggalLahir',$data['tanggalLahir']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    public function rmvTadik($id){
        $sql = "DELETE FROM pesertaDidik WHERE idxTadik = :id"; 
        $this->db->query($sql);
        $this->db->bind('id',$id);
        $this->db->execute();
        return $this->db->rowCount();
    }

    public function dftTadik($sessionID,$pn=1){
        $row = ( $pn -1 ) * rows;
        $sql ="SELECT idxTadik,namaLengkap,gender,tempatLahir,DATE_FORMAT(tanggalLahir,'%d-%m-%Y') tanggalLahir FROM " . $this->table . " WHERE sessionID = :sessionID ORDER BY idxTadik LIMIT $row ," . rows;

        $this->db->query($sql);
        $this->db->bind('sessionID',$sessionID);
        return $this->db->resultSet();
    }

    public function dtlTadik($idxTadik){
        // $row = ( $pn -1 ) * rows;
        $sql ="SELECT  * FROM pesertaDidik WHERE idxTadik = :idxTadik ORDER BY idxTadik LIMIT 1";
        $this->db->query($sql);
        $this->db->bind('idxTadik',$idxTadik);
        return $this->db->resultOne();
    }

    public function setMasTadik($ids,$ipd,$nl,$jk,$tl){
        $sql = "INSERT INTO pesertaDidik SET sessionID=:ids , idxTadik=:ipd, namaLengkap =:nl , gender=:jk ,tempatLahir =:tl ";
        $this->db->query($sql);
        $this->db->bind('ids',$ids);
        $this->db->bind('ipd',$ipd);
        $this->db->bind('nl',$nl);
        $this->db->bind('jk',$jk);
        $this->db->bind('tl',$tl);
        $this->db->execute();
        return $this->db->rowCount();
    }


}
