<?php
class Model_rayahan
{
    // private $table = "nama tabel";
    private $db;

    public function __construct()
    {
        $this->db = new Database();
    }

    
    public function kolomPenerimaTransfer(){
        $kolom = ['NOMOR','NAMA PENERIMA','KODE NPWP','NAMA SESUAI REKENING TABUNGAN','NAMA BANK','NOMOR REKENING','NILAI RUPIAH','KODE BANK','KODE BANK SPAN','KODE SWIFT','KODE POS','KODE NEGARA','KODE KPPN','TIPE SUPLIER','KOTA','PROVINSI','EMAIL','TELEPON','KODE IBAN','ALAMAT PENERIMA','KODE NEGARA','ASAL BANK','KODE PROVINSI','KODE KAB/KOTA','NRS','NIP'];

        return $kolom;
    }

    public function kolomTutorBantu(){
        $kolom = ['NOMOR URUT','NAMA PENERIMA','KODE NPWP','NAMA BANK','NOMOR REKENING','NILAI RUPIAH','KOTA'];

        return $kolom;
    }

    public function appheader(){
        echo "<h1>
        PUSAT PENGEMBANGAN PENDIDIKAN ANAK USIA DINI DAN PENDIDIKAN MASYARAKAT<br>
        PROVISI JAWA TENGAH
        </h1>";
    }

    public function printBulan($bulan){
        $nabul = [
            '01'=>'Januari',
            '02'=>'Februari',
            '03'=>'Maret',
            '04'=>'April',
            '05'=>'Mei',
            '06'=>'Juni',
            '07'=>'Juli',
            '08'=>'Agustus',
            '09'=>'September',
            '10'=>'Oktober',
            '11'=>'Nopember',
            '12'=>'Desember'
        ];

        list($t,$b) = explode("-",$bulan);
        $str = $nabul[$b].' '.$t;
        return $str;
    }

    public function pagination($m,$n=1){
    if($n == 1){
        $next = $n + 1;
        $prev = 1;
    }else{
        $next = $n + 1;
        $prev = $n - 1;
    }

    return " 
    <div class='row'>
        <div class='col-md-12'>
            <nav aria-label='pageNav' class='float-right mx-3'>
                <ul class='pagination'>
                    <li class='page-item'><a class='page-link' href='".BASEURL."$m/$prev'>&lt;&lt;</a></li>
                    <li class='page-item'><a class='page-link' href='#'>".$n."</a></li>
                    <li class='page-item'><a class='page-link' href='".BASEURL."$m/$next'>&gt;&gt;</a></li>
                </ul>
            </nav>
        </div>
    </div>";
    }

    public function randomCredential(){
        $usernames=['amungme','devayan','fordata','haloban','korowai','lampung','makasar','morotai','sigulai','singkil','sumbawa','tamiang','tengger','Ternate'];

        $passwords = ['aries','canes','canis','canis','cetus','draco','hydra','indus','lepus','libra','lupus','mensa','musca','norma','orion','pyxis','virgo'];

        $urand = mt_rand(0,14); $prand=mt_rand(0,16);
        $unumb = mt_rand(111,199); $pnumb = mt_rand(555,655);

        $username = $usernames[$urand].$unumb;
        $password = $passwords[$prand].$pnumb;

        return ['username'=>$username,'password'=>$password];
    }

    public function genTbSess($n){
        $id = sprintf('%04d',$n);
        //$cp = ['S','A','R','U','N','G','P','E','C','I'];
        
        $cp = [
            ['S','A','R','U','N','G','P','E','C','I'],
            ['M','A','I','N','F','O','L','D','E','R'],
            ['W','A','R','U','N','G','K','O','P','I'],
            ['R','O','U','N','D','T','A','B','L','E']
        ];
        
        $sessionID = '';
        for( $i=0; $i < strlen($id) ; $i++ ){
            $x = $id[$i];
            $sessionID.=$cp[$i][$x];
        }
        $cek = $this->checkUsage($sessionID);
        if( $cek['kelas'] > 0 ){
            return $this->genTbSess($n+1);
            // $x=$n1;
            // $this->genTbSess($x);
        }else{
            return $sessionID;
        }
    }

    private function checkUsage($sessID){
        $sql = "SELECT COUNT(sessionID) AS kelas FROM tbSession WHERE sessionID=:sessID";
        $this->db->query($sql);
        $this->db->bind('sessID',$sessID);
        return $this->db->resultOne();
    }

}
