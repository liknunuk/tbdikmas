<?php
// sesuaikan nama kelas, tetap extends ke Controller
class Home extends Controller
{
  // method default
  public function index()
  {
    $data['title']="Tutor Bantu Jawa Tengah";
    $this->view('template/home-header',$data);
    $this->view('home/index',$data);
    $this->view('template/footer');
  }

  public function login(){
    
    $tb = $this->model('Model_tutors')->tbauth($_POST);
    if( $tb != NULL ){
      // setSessions
      $_SESSION['tb'] = sprintf('%03d',$tb['nomor']);
      $_SESSION['namaPenerima'] = $tb['namaPenerima'];
      $_SESSION['program'] = $tb['program'];
      $_SESSION['kota'] = $tb['kota'];
      header('Location:' . BASEURL . "Tutor" );
    }else{
      header('Location:' . BASEURL  );
    }
  }

  public function logout(){
    session_unset($_SESSION);
    session_destroy();
    header('Location:' . BASEURL  );
  }

  public function loginPermit(){
    $data['title'] = "Request Login Data";
    $this->view('template/home-header',$data);
    $this->view('home/matching');
    $this->view('template/footer');
  }

  public function uvrf(){
    $data['title']='Verifikasi User';
    $data['tb']=$this->model('Model_tutors')->matchTB($_POST);
    $data['uname']=$this->model('Model_tempUser')->randUsername();
    $data['upass']=$this->model('Model_tempUser')->randPassword();

    $this->view('template/home-header',$data);
    $this->view('home/tbverify',$data);
    $this->view('template/footer');
  }
  
  public function setCred(){
    $data['title']="Konfirmasi Login";
    $data['login']=$_POST;
    if( $this->model('Model_tutors')->credReset($_POST) > 0 ){
      
      $this->view('template/home-header',$data);
      $this->view('home/setCred',$data);
      $this->view('template/footer');

    }else{
      echo "Update gagal"; exit();
    }
  }

  // public function registrasi(){
  //   $data['title']="Tutor Bantu Jawa Tengah";
  //   $this->view('template/home-header',$data);
  //   $this->view('home/registrasi');
  //   $this->view('template/footer');
  // }
  
  // public function ctbconfirm(){
  //   $data['title']="Konfirmasi Data";
  //   $data['uname'] = $this->model('Model_tempUser')->randUsername();
  //   $data['upass'] = $this->model('Model_tempUser')->randPassword();
  //   $data['ktb'] = $this->model('Model_tempUser')->randKtb();
  //   $data['ctb'] = $_POST;
  //   $this->view('template/home-header',$data);
  //   $this->view('home/tbconfirm',$data);
  //   $this->view('template/footer');
  // }

  // public function setUser(){
  //   if( $this->model('Model_tempUser')->setUser($_POST) > 0 ){
  //     $data['uname'] = $_POST['tbUsername'];
  //     $data['upass'] = $_POST['tbPassword'];
  //     $this->view('template/home-header',$data);
  //     $this->view('home/note',$data);
  //     $this->view('template/footer');
  //   }
  // }

  public function kelalen(){
    $data['title']="Sori aku lali";
    $data['kota']=$this->model('Model_tutors')->kotatb();
    $this->view('template/home-header',$data);
    $this->view('home/kelalen',$data);
    $this->view('template/footer');
  }

  public function tutore(){
    $data['tutor']=$this->model('Model_tutors')->tbKota($_POST['kota']);
    echo json_encode($data['tutor'],JSON_PRETTY_PRINT);
  }

  public function bosen(){
    $passwordBaru = md5($_POST['email'].'++'.$_POST['password']);
    $data = ['tbUsername'=>$_POST['email'], 'tbPassword'=>$passwordBaru , 'ktb'=>$_POST['ktb']];
    if($this->model('Model_tutors')->bosenlogin($data) > 0 ){
      header("Location:" . BASEURL );
    }
  }
}

/*
  jurus dapat kolom dan binding kolom
  foreach($_POST AS $key=>$val){
    echo "$key = :$key , ";
  }
  // binding
  foreach($_POST AS $key=>$val){
    echo "\$this->db->bind('$key',\$data['$key']);<br/>";
  }
*/
