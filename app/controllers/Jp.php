<?php

class Jp extends Controller
{
    public function index()
    {

        $data['title'] = "Input Jampel";
        $this->view('template/header-tb', $data);
        $this->view('tutor/jampel', $data);
        $this->view('template/footer');
    }

    public function jurnal()
    {
        // $bulan = $bulan == false ? date('Y-m') : $bulan;
        // 'jpjur' => $this->model('Model_jourgi')->rekapJpJurnal($bulan),
        $data = [
            'title' => 'Rekap Jampel',
            'tutor' => $this->model('Model_tutors')->allTutor()
        ];

        $this->view('template/header-tb', $data);
        $this->view('tutor/jpjurnal', $data);
        $this->view('template/footer');
    }

    public function cekJpJurnal($nomor, $bulan)
    {
        $data = $this->model('Model_jourgi')->tutorJpJurnal($nomor, $bulan);
        if (empty($data['jampel'])) {
            echo "0";
        } else {
            echo $data['jampel'];
        }
    }

    public function tbData($ktb)
    {
        $tb = $this->model('Model_tutors')->tutorByKtb($ktb);
        echo "
        <table border='0'>
            <tbody>
                <tr>
                    <td>Nama Tutor</td>
                    <td>" . $tb['namaPenerima'] . "</td>
                </tr>
                <tr>
                    <td>Program</td>
                    <td>" . $tb['program'] . "</td>
                </tr>
                <tr>
                    <td>SKB Kota/Kab.</td>
                    <td>" . $tb['kota'] . "</td>
                </tr>
            </tbody>
        </table>
        ";
    }

    public function setJampel()
    {
        $_POST['ktb'] = $_POST['program'] . sprintf("%03d", $_POST['nmUrut']);

        if ($this->model('Model_tutors')->setorJP($_POST) > 0) {
            Alert::setAlert('berhasil disimpan', 'Setoran Jam Pelajaran', 'success');
            header("Location:" . BASEURL . "Jp");
        }
    }

    public function rekap($bulan = "")
    {
        $bulan = $bulan == "" ? date('Y-m') : $bulan;
        $data['title'] = "Rekap Jam Pelajaran";
        $data['rekjp'] = $this->model('Model_pppaud')->rekapJampel($bulan);
        $data['bulan'] = $bulan;

        $this->view('template/header-tb', $data);
        $this->view('p2paud/rjampel', $data);
        $this->view('template/footer');
    }
}
