<?php
// sesuaikan nama kelas, tetap extends ke Controller
class Tutor extends Controller
{

  public function __construct()
  {
    // $_SESSION['tb'] = sprintf('%03d',33);
  }
  // method default
  public function index()
  {
    $data['title'] = 'Tutor Bantu Jateng ' . date('Y');
    $data['tb'] = $this->model('Model_tutors')->dataTutor($_SESSION['tb']);
    $data['session'] = $this->model('Model_tbsession')->activeSession($_SESSION['tb']);
    $data['berita'] = $this->model('Model_informasi')->infoTerkini();
    // cek credential
    $pref   = $_SESSION['program'] == "KESETARAAN" ? 'SET' : 'AUD';
    $uname  = $pref . $_SESSION['tb'];
    if ($uname == $data['tb']['sesulih']) header("Location:" . BASEURL . "Tutor/membamemba");

    $this->view('template/header-tb', $data);
    $this->view('tutor/index', $data);
    $this->view('template/footer');
  }

  public function sulihasma()
  {
    $data['ktb'] = $this->model('Model_tutors')->myktb($_SESSION['tb']);
    $this->view('template/header-tb', $data);
    $this->view('tutor/frChPass', $data);
    $this->view('template/footer');
  }

  public function membamemba()
  {
    // print_r($_SESSION);
    $data['kota'] = $this->model('Model_tutors')->kotatb();
    $this->view('template/header-tb', $data);
    $this->view('home/kelalen', $data);
    $this->view('template/footer');
  }

  public function kuncibaru()
  {
    if ($this->model('Model_tutors')->credReset($_POST) > 0) {
      Alert::setAlert('Berhasil diubah', 'Data login', 'success');
    } else {
      Alert::setAlert('Gagal diubah', 'Data login', 'danger');
    }
    header("Location:" . BASEURL . "Tutor");
  }

  public function berita($idx)
  {

    $data['title'] = 'Tutor Bantu Jateng ' . date('Y');
    $data['tb'] = $this->model('Model_tutors')->dataTutor($_SESSION['tb']);
    $data['session'] = $this->model('Model_tbsession')->activeSession($_SESSION['tb']);
    $data['berita'] = $this->model('Model_informasi')->detilInfo($idx);

    $this->view('template/header-tb', $data);
    $this->view('tutor/berita', $data);
    $this->view('template/footer');
  }

  public function kelasBaru($id = "")
  {
    $nomorSession = $this->model('Model_tbsession')->sessionNumber() + 1;
    $data['tb'] = $this->model('Model_tutors')->dataTutor($_SESSION['tb']);
    $data['session'] = $this->model('Model_tbsession')->activeSession($_SESSION['tb']);
    $data['sessionID'] = $this->model('Model_rayahan')->genTbSess($nomorSession);
    $data['title'] = "Kelas Tutor Bantu";
    $data['mod'] = $id == "";

    $this->view('template/header-tb', $data);
    $this->view('tutor/frKelas', $data);
    $this->view('template/footer');
  }

  public function setSession()
  {
    // print_r($_POST);
    if ($this->model('Model_tbsession')->setSession($_POST) > 0) {
      Alert::setAlert('Berhasil dibuat', 'Kelas baru', 'success');
      header("Location:" . BASEURL . 'Tutor/');
    } else {
      Alert::setAlert('Berhasil dibuat', 'Kelas baru', 'success');
      header("Location:" . BASEURL . 'Tutor/');
    }
  }

  public function tadik($kelas = "", $pn  = 1)
  {
    $data['title'] = 'Peserta Didik';
    if ($pn == 1) {
      $data['pp'] = 1;
      $data['np'] = 2;
    } else {
      $data['pp'] = $pn - 1;
      $data['np'] = $pn + 1;
    }
    // $data['tb'] = $this->model('Model_tutors')->dataTutor($_SESSION['tb']);
    $data['session'] = $this->model('Model_tbsession')->activeSession($_SESSION['tb']);
    if ($kelas != "") {
      $data['tadik'] = $this->model('Model_gabel')->dftTadik($kelas, $pn);
      $data['kelas'] = $kelas;
    } else {
      $data['tadik'] = NULL;
      $data['kelas'] = NULL;
    }

    $this->view('template/header-tb', $data);
    $this->view('tutor/tadik', $data);
    $this->view('template/footer');
  }

  public function frTadik($kls, $mod = 'baru', $id = '')
  {
    $data['title'] = 'Peserta Didik Baru';
    $data['mod'] = $mod;
    $data['tb'] = $this->model('Model_tutors')->dataTutor($_SESSION['tb']);
    // $data['session'] = $this->model('Model_tbsession')->activeSession($_SESSION['tb']);
    $data['kelas'] = $kls;
    if ($mod == 'baru') {
      // $urut = $this->model('Model_gabel')->urutBaru($_SESSION['tb'].'-'.$data['session']['sessionID']);
      $urut = $this->model('Model_gabel')->urutBaru($_SESSION['tb'] . '-' . $kls);
      $data['wb'] = [
        'sessionID' => $kls,
        'idxTadik' => $_SESSION['tb'] . '-' . $kls . '-' . $urut,
        'namaLengkap' => '',
        'gender' => '',
        'tempatLahir' => '',
        'tanggalLahir' => ''
      ];
    } else {
      $data['wb'] = $this->model('Model_gabel')->dtlTadik($id);
    }

    $this->view('template/header-tb', $data);
    $this->view('tutor/frTadik', $data);
    $this->view('template/footer');
  }

  public function setGabel()
  {
    if ($_POST['mod'] == 'baru') {

      if ($this->model('Model_gabel')->newTadik($_POST) > 0) {
        Alert::setAlert('berhasil disimpan', 'Data peserta didik', 'success');
      } else {
        Alert::setAlert('gagal disimpan', 'Data peserta didik', 'warning');
      }
    } else {

      if ($this->model('Model_gabel')->chgTadik($_POST) > 0) {
        Alert::setAlert('berhasil dimutakhirkan', 'Data peserta didik', 'success');
      } else {
        Alert::setAlert('gagal dimutakhirkan', 'Data peserta didik', 'warning');
      }
    }

    header('Location:' . BASEURL . 'Tutor/tadik/' . $_POST['sessionID']);
  }

  public function tadikMutasi()
  {
    if ($this->model('Model_gabel')->rmvTadik($_POST['idxTadik']) > 0) {
      Alert::setAlert('berhasil dihapuskan', 'Data peserta didik', 'success');
    } else {
      Alert::setAlert('gagal dihapuskan', 'Data peserta didik', 'warning');
    }
    header('Location:' . BASEURL . 'Tutor/tadik');
  }

  public function jourgi($kelas = NULL, $pn = 1)
  {

    $data['title'] = 'Jurnal Kegiatan';
    $data['tb'] = $this->model('Model_tutors')->dataTutor($_SESSION['tb']);
    $data['session'] = $this->model('Model_tbsession')->activeSession($_SESSION['tb']);
    if ($kelas != "") {
      $data['jurnal'] = $this->model('Model_jourgi')->daftarJurnal($kelas, $pn);
      $data['kelas'] = $kelas;
      $data['nmKelas'] = $this->model('Model_tbsession')->sessionInfo($kelas);
    } else {
      $data['jurnal'] = NULL;
      $data['kelas'] = NULL;
      $data['nmKelas']['sessionName'] = "<p class='text-danger'>Pilih kelas dahulu!</p>";
    }
    $data['pn'] = $pn;

    $this->view('template/header-tb', $data);
    $this->view('tutor/jourgi', $data);
    $this->view('template/footer');
  }

  public function frJural($mod = 'baru')
  {
    $data['title'] = 'Jurnal Kegiatan';
    $data['mod'] = $mod;
    $data['tb'] = $this->model('Model_tutors')->dataTutor($_SESSION['tb']);
    $data['session'] = $this->model('Model_tbsession')->activeSession($_SESSION['tb']);

    $this->view('template/header-tb', $data);
    $this->view('tutor/frJurnal', $data);
    $this->view('template/footer');
  }

  public function setJurnal()
  {
    // print_r($_POST);
    $rowCount = $this->model('Model_jourgi')->nambahJurnal($_POST);
    // echo "Row Affected: " . $rowCount;
    if ($rowCount > 0) {
      Alert::setAlert('berhasil disimpan', 'Data Jurnal', 'info');
    } else {
      Alert::setAlert('gagal disimpan', 'Data Jurnal', 'warning');
    }
    header('Location:' . BASEURL . 'Tutor/jourgi/' . $_POST['sessionID']);
  }

  public function jurnas()
  {
    // jurnal nas ( wurung , ora sida )
    $hapus = $this->model('Model_jourgi')->mbuangJurnal($_POST['idxJurnal']);
    echo $hapus;
  }

  public function massjourn($kelas)
  {
    $data['kelas'] = $kelas;
    $data['nmKelas'] = $this->model('Model_tbsession')->sessionInfo($kelas);
    $this->view('tutor/frJournalMass', $data);
  }

  public function setMassJournal()
  {
    // sessionID , tanggal , jamMulai , jamAkhir , kegiatan
    $err = 0;
    foreach ($_POST['data'] as $data) {
      $jurnal = ['sessionID' => $_POST['sessionID'], 'tanggal' => $data[0], 'jamMulai' => $data[1], 'jamAkhir' => $data[2], 'kegiatan' => $data['3']];
      if ($this->model('Model_jourgi')->nambahJurnal($jurnal) > 0) {
        $err += 0;
      } else {
        $err += 1;
      }
    }
    echo $err == 0 ? "1" : "0";
  }

  public function presensi($sesi, $pn = 1, $mod = 'baru')
  {
    $data['title'] = 'Jurnal Kegiatan';
    // $data['mod'] = $mod;
    $data['tb'] = $this->model('Model_tutors')->dataTutor($_SESSION['tb']);
    $data['session'] = $this->model('Model_tbsession')->activeSession($_SESSION['tb']);
    $data['sesi'] = $sesi;
    $data['jurnal'] = $this->model('Model_jourgi')->detailJurnal($sesi);

    $data['tadik'] = $this->model('Model_presensi')->pesertaSession($data['jurnal']['sessionID']);

    $this->view('template/header-tb', $data);
    $this->view('tutor/presensi', $data);
    $this->view('template/footer');
  }

  public function setPresensi()
  {
    // print_r($_POST);
    $response = $this->model('Model_presensi')->setPresensi($_POST);
    echo $response;
  }

  public function dhTadik($kelas = "")
  {
    $data['title'] = 'Daftar Hadir Kegiatan';
    $data['tb'] = $this->model('Model_tutors')->dataTutor($_SESSION['tb']);
    $data['session'] = $this->model('Model_tbsession')->activeSession($_SESSION['tb']);
    if ($kelas != "") {
      $data['dh'] = $this->model('Model_presensi')->dftPresensi($kelas);
    } else {
      $data['dh'] = NULL;
    }
    $this->view('template/header-tb', $data);
    $this->view('tutor/dftpresensi', $data);
    $this->view('template/footer');
  }

  public function hadirin($id)
  {
    $data['pd'] = $this->model('Model_presensi')->hadirin($id);
    $this->view('tutor/dfthadirin', $data);
  }

  public function lapor($id = "")
  {
    if ($id == "") {
      $data['tblap'] = [
        'tipelap' => '', 'bulan' => '', 'tanggal' => '', 'dgLink' => ''
      ];
    } else {
      $data['tblap'] = $this->model('Model_laporan')->laporanTB($id);
    }
    $data['idlap'] = $id;
    $data['title'] = 'Laporan Bulanan Tutor Bantu';
    $data['tb'] = $this->model('Model_tutors')->dataTutor($_SESSION['tb']);
    $data['session'] = $this->model('Model_tbsession')->activeSession($_SESSION['tb']);
    $data['laporan'] = $this->model('Model_laporan')->dftLaporan($_SESSION['tb']);
    $this->view('template/header-tb', $data);
    $this->view('tutor/laporan', $data);
    $this->view('template/footer');
  }

  public function setLaporan()
  {

    if ($_POST['mod'] == "baru") {
      if ($this->model('Model_laporan')->setLaporan($_POST) > 0) {
        Alert::setAlert('berhasil terkirim', 'Laporan Tutor', 'success');
      } else {
        Alert::setAlert('gagal terkirim', 'Laporan Tutor', 'danger');
      }
    } else {
      if ($this->model('Model_laporan')->chgLaporan($_POST) > 0) {
        Alert::setAlert('berhasil diupdate', 'Laporan Tutor', 'success');
      } else {
        Alert::setAlert('gagal diupdate', 'Laporan Tutor', 'danger');
      }
    }
    header("Location:" . BASEURL . "Tutor/Lapor/");
  }

  public function cabutLaporan()
  {
    if ($this->model('Model_laporan')->rmvLaporan($_POST['id']) > 0) {
      echo 1;
    } else {
      echo 0;
    }
  }

  public function liquidasi()
  {
    //print_r( $_SESSION );
    $data['title'] = 'Laporan Bulanan Tutor Bantu';
    $data['tb'] = $this->model('Model_tutors')->dataTutor($_SESSION['tb']);
    $data['session'] = $this->model('Model_tbsession')->activeSession($_SESSION['tb']);
    $data['transfer'] = $this->model('Model_tutors')->dfTrx($_SESSION['tb']);
    $this->view('template/header-tb', $data);
    $this->view('tutor/trxhonor', $data);
    $this->view('template/footer');
  }

  public function chgIdtty()
  {
    $data['title'] = "IDENTITAS TUTOR BANTU";
    $data['tb'] = $this->model('Model_tutors')->dataTutor($_SESSION['tb']);

    $this->view('template/header-tb', $data);
    $this->view('tutor/frTutor', $data);
    $this->view('template/footer');
  }

  public function SetIdtty()
  {
    if ($this->model('Model_tutors')->tutorUbah($_POST) > 0) {
      Alert::setAlert('berhasil diubah', 'Profil tutor', 'success');
    } else {
      Alert::setAlert('gagal diubah', 'Profil tutor', 'danger');
    }

    header("Location:" . BASEURL . "Tutor");
  }

  public function rombel($id)
  {
    $data['title'] = 'Tutor Bantu Jateng ' . date('Y');
    $data['tb'] = $this->model('Model_tutors')->dataTutor($_SESSION['tb']);
    $data['session'] = $this->model('Model_tbsession')->activeSession($_SESSION['tb']);
    $data['sesinfo'] = $this->model('Model_tbsession')->sessionInfo($id);


    $this->view('template/header-tb', $data);
    $this->view('tutor/rombel', $data);
    $this->view('template/footer');
  }

  public function chgrombel()
  {
    if ($this->model('Model_tbsession')->chgSession($_POST) > 0) {
      header("Location:" . BASEURL . "Tutor/rombel/" . $_POST['sessionID']);
    } else {
      echo $_POST['sessionID'];
    }
  }

  public function remrombel()
  {
    // echo "remove rombel";
    // echo $_POST['sessionID'];
    if ($this->model('Model_tbsession')->removeRombel($_POST) > 0) {
      echo "1";
    } else {
      echo "0";
    }
  }

  public function massTadik($id)
  {
    $data['objek'] = 'rombel';
    $data['kelas'] = $id;
    $this->view('tutor/frTadikMas', $data);
  }

  public function setMassStudents()
  {
    // echo json_encode($_POST);
    $err = 0;
    foreach ($_POST['data'] as $data) {
      $idxTadik = $_SESSION['tb'] . '-' . $_POST['sessionID'] . '-' . sprintf('%03d', $data[0]);
      if ($this->model('Model_gabel')->setMasTadik($_POST['sessionID'], $idxTadik, $data[1], $data[2], $data[3]) > 0) {
        $err += 0;
      } else {
        $err += 1;
      }
    }
    if ($err == 0) {
      echo "1";
    } else {
      echo "0";
    }
  }

  public function gapres()
  {
    if ($this->model('Modelpresensi')->rasidoteko($_POST) > 0) {
      echo "1";
    } else {
      echo "0";
    }
  }
}
