<?php
// sesuaikan nama kelas, tetap extends ke Controller
class P2paud extends Controller
{
  // method default
  public function index($pn = 1)
  {
    $data['title']='P2PAUDDIKMASJTENG';
    $data['kolom'] = $this->model('Model_rayahan')->kolomTutorBantu();
    $data['tutors'] = $this->model('Model_tutors')->daftarTutor($pn);
    $data['pn'] = $pn;
    $data['method']='P2paud';
    $data['pagina'] = $this->model('Model_rayahan')->pagination("P2paud",$pn);

    $this->view('template/header',$data);
    $this->view('p2paud/index',$data);
    $this->view('template/footer');
  }

  public function tbJampel($tbid,$bulan=''){
    $data['bulan'] = $bulan == '' ? date('Y-m') : $bulan;
    $data['title'] = 'Japel By Jurnal';
    $data['jampel']= $this->model('Model_jourgi')->jpjurnal($tbid,$bulan);
    $data['majurn']= $this->model('Model_jourgi')->masaJurnalAktif($tbid);
    $data['tbid'] = $tbid;

    $this->view('template/header',$data);
    $this->view('p2paud/jpjurnal',$data);
    $this->view('template/footer');
  }

  public function transfer($group='SP',$bulan=''){
    $data['bulan'] = $bulan == '' ? date('Y-m') : $bulan;
    $data['title'] = 'Honorarium';
    $data['penerima'] = $this->model('Model_tutors')->tutorByGroup($group);
    $data['method']='P2paud';
    $data['program'] = $group;

    $this->view('template/header',$data);
    $this->view('p2paud/pencairan',$data);
    $this->view('template/footer');
  }

  public function setTransfer(){
    // print_r($_POST);
    $setTransfer = $this->model('Model_pppaud')->setTransfer($_POST);
    echo $setTransfer;
  }

  public function buktiTransfer($bulan="",$program=""){
    if($bulan == ""){$bulan   = date('Y-m');}
    if($program==""){$program = '%';}

    $data['title'] = 'Bukti Transfer';
    $data['bulan'] = $this->model('Model_rayahan')->printBulan($bulan);
    $data['kolomTabel'] = $this->model('Model_rayahan')->kolomPenerimaTransfer();
    $data['program'] = $program == '%' ? "Semua program" : $program;
    $data['transfer'] = $this->model('Model_pppaud')->buktiTransfer($bulan,$program);

    $this->view('template/header',$data);
    $this->view('p2paud/lapTransfer',$data);
    $this->view('template/footer');
  }

  public function penerimaBulanan($idxTrf){
    $data['penerima'] = $this->model('Model_pppaud')->penerimaBulanan($idxTrf);
    $data['kolomTabel'] = $this->model('Model_rayahan')->kolomPenerimaTransfer();
    $this->view('p2paud/penerimaTrf',$data);
  }

  public function exportPenerima($idxTrf){
    $data['trf'] = $this->model('Model_pppaud')->bulanTransfer($idxTrf);
    $data['penerima'] = $this->model('Model_pppaud')->penerimaBulanan($idxTrf);
    $data['kolomTabel'] = $this->model('Model_rayahan')->kolomPenerimaTransfer();
    $this->view('p2paud/exportPenerima',$data);
  }

  public function tbform($mod = "enggal",$tbId=""){
    if($tbId != ""){
      $data['tutor'] = $this->model('Model_tutors')->dataTutor($tbId);
    }else{
      $data['tutor']=[
        'program'=>'','namaPenerima'=>'','kodeNPWP'=>'','namaRekTab'=>'','namaBank'=>'','nomorRekening'=>'','nilaiRupiah'=>'','Kota'=>'','nomor'=>NULL
      ];
    }
    $data['title'] = "Tutor Bantu";
    $data['mod']=$mod;
    $data['cred']=$this->model('Model_rayahan')->randomCredential();

    $this->view('template/header',$data);
    $this->view('p2paud/tbbaru',$data);
    $this->view('template/footer');
  }

  public function setTutor(){
    if($_POST['mod'] == 'enggal'){

      if( $this->model('Model_tutors')->tutorBaru($_POST) > 1 ){
        Alert::setAlert('berhasil ditambahkan','Data tutor','success');
      }

    }elseif($_POST['mod']=='gantos'){

      if( $this->model('Model_tutors')->tutorLama($_POST) > 1 ){
        Alert::setAlert('berhasil dimutakhirkan','Data tutor','success');
      }

    }else{
      header("Location:".BASEURL."P2paud");
    }
    header("Location:".BASEURL."P2paud");
  }

  public function tbfired(){
    $hapus = $this->model('Model_tutors')->tbfired($_POST['tbid']);
    echo $hapus;
  }

  public function tbinfo($idx=""){
    $data['title']="Informasi Tutor Bantu";
    $data['berita'] = $this->model('Model_informasi')->infoTerkini();
    $data['mod'] = $idx == "" ? 'baru':'lama';
    if($idx == ""){
      $data['kabar']=[
        'idxInfo'=>md5('1000-'.date('Y-m-d')),
        'author'=>'P2PAUDDIKMAS JATENG',
        'waktu'=>date('Y-m-d'),
        'judul'=>'',
        'berita'=>''
      ];
    }else{
      $data['kabar']=$this->model('Model_informasi')->detilInfo($idx);
    }
    
    $this->view('template/header',$data);
    $this->view('p2paud/informasi',$data);
    $this->view('template/footer');
  }

  public function srcTutorRslt($name){
    $dataTutors = $this->model('Model_tutors')->tutorByNames($name);
    echo json_encode($dataTutors,JSON_PRETTY_PRINT);
  }

  public function setInfo(){
    if( $this->model('Model_informasi')->setInfo($_POST) >0 ){
      
      if($_POST['mod'] == 'baru'){
        Alert::setAlert('berhasil disimpan','Informasi tutor bantu', 'success');
      }else{
        Alert::setAlert('berhasil diupdate','Informasi tutor bantu', 'info');
      }
        
    }
    header("Location:".BASEURL."P2paud/tbinfo");
  }

  public function tblabul($bulan=""){
    $bulan = $bulan == "" ? date('Y-m') : $bulan;

    $data['title'] = "Laporan Bulanan";
    $data['labul'] = $this->model('Model_laporan')->labul($bulan);
    $data['bulan'] = $bulan;
    
    $this->view('template/header',$data);
    $this->view('p2paud/lapbultb',$data);
    $this->view('template/footer');
  }

  public function login(){
    $data['title']="PP PAUD DIKMAS JATENG";
    $this->view('p2paud/login',$data);
  }
  
  public function auth(){
    $loginData = md5($_POST['username'] . "*X*" . $_POST['password']);
    $keys = [
      '457c59a7c24e13fb50d09d11e35ab92b','6b1b9fcf132d3180345f5c346e8557f6','457d402211dc06d35e66b3d72e516fd1'
    ];
    
    if( in_array($loginData,$keys)){
      $_SESSION['admin']=$_POST['username'];
      header("Location:" . BASEURL . "P2paud");
    }
  }

  public function logout(){
    session_unset($_SESSION);
    session_destroy();
    header("Location:" . BASEURL );
  }
}
