<?php $home=BASEURL."Tutor/";?>

<div class="container-fluid">
  <?php Alert::sankil(); ?>
  <div class="row">
    <div class="col-lg-10">
      <?php
      if($data['session'] != NULL ){
        $this->view('tutor/tbnav');
      }
      ?>
      <div class="card">
          <div class="card-header row">
            <div class="col-md-10">
                <h4>Kehadiran Peserta Didik<br/>
                <small>
                    Kelas: <?=$data['jurnal']['sessionName'];?>.<br/> Kegiatan: <?=$data['jurnal']['kegiatan'];?>
                </small>
                </h4>
            </div>
            <div class="col-md-2">
                <!-- <a class="btn btn-primary" data-toggle="collapse" href="#frJurnal" role="button" aria-expanded="false" aria-controls="frJurnal">
                    + Kegiatan
                </a> -->
                &nbsp;
            </div>
          </div>
          <div class="card-body" id="frJurnal">

            <div class="form-horizontal">

                <div class="from-group row">
                    <label for="tanggal" class="col-md-4">Tanggal</label>
                    <div class="col-md-8">
                        <input type="date" name="tanggal" id="tanggal" class="form-control" readonly value="<?=$data['jurnal']['tanggal'];?>">
                    </div>
                </div>

                <div class="from-group row">
                  <label for="tadik" class="col-sm-4">
                  Peserta didik
                    <span class="float-right">
                      <input type="checkbox" id="allTadik">&nbsp; Pilih Semua
                    </span>
                  </label>
                  <div class="col-sm-8 row">

                    <?php foreach($data['tadik'] as $pd ): ?>
                    <div class="col-sm-4">
                      <span><input type="checkbox" name="idxTadik" value="<?=$pd['idxTadik'];?>" class="idxTadik"> <?=$pd['namaLengkap'];?></span>
                    </div>
                    <?php endforeach; ?>
              
                  </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-12 d-flex justify-content-end mt-2">
                        <button id="savePresensi" class="btn btn-success">Simpan</button>
                    </div>
                </div>

            </div>

          </div>
      </div>
    </div>
    <div class="col-lg-2">
      <?php $this->view('tutor/tbIdentity',$data);?>
    </div>
    
  </div>
</div>
<?php $this->view('template/bs4js');?>
<script>
$('#allTadik').change( function(){
  if(this.checked == true){
    $('.idxTadik').prop('checked',true);
  }else{
    $('.idxTadik').prop('checked',false);
  }
})

$("#savePresensi").click(function(){
  let hadirin='';
  $.each( $(".idxTadik:checked") , function(){
    hadirin+=this.value+",";
  })
  console.log(hadirin);
  $.post('<?=BASEURL;?>Tutor/setPresensi',{
    sessionID:'<?=$data['jurnal']['sessionID'];?>',
    idxJurnal:'<?=$data['sesi'];?>',
    hadirin: hadirin
  },function(resp){
    console.log(resp);
    if( resp == "1" ){
      window.location="<?=$home;?>dhTadik/<?=$jurnal['sessionID'];?>";
    }
  })
})
</script>