<div class="container-fluid">
  <?php Alert::sankil(); ?>
  <div class="row">
    <div class="col-lg-10">
      <?php
      if($data['session'] != NULL ){
        $this->view('tutor/tbnav');
      }
      ?>
      <div class="card">
        <div class="card-header page-title">
            <div class="col-lg-12">
                <h3>Daftar Pencairan Honorarium</h3>
            </div>
        </div>
        <div class="card-body table-responsive">
            <table class="table table-striped">
                <thead>
                    <tr class='text-center font-weight-bold'>
                        <th>Nomor</th>
                        <th>Bulan</th>
                        <th>Pencatatan Transfer</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $nu = 1; foreach($data['transfer'] as $trx ): ?>
                    <tr>
                        <td class='text-right'><?=$nu;?>.</td>
                        <td><?=$trx['bulan'];?></td>
                        <td><?=$trx['waktu'];?></td>
                    </tr>
                    <?php $nu++; endforeach; ?>
                
                </tbody>
            </table>
          
        </div>
      </div>
    </div>
    <div class="col-lg-2">
      <?php $this->view('tutor/tbIdentity',$data);?>
    </div>
    
  </div>
</div>
<pre>
<?php
?>
</pre>

<?php $this->view('template/bs4js');?>
<script>
$("#setNewSession").click(function(){
  window.location.href="<?=BASEURL;?>Tutor/kelasBaru";
})
</script>