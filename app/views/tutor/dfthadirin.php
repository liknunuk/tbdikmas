<?php $home=BASEURL."Tutor/"; $nurut =1;?>
<table class="table table-striped table-sm">
    <thead>
        <th>Nomor Urut</th>
        <th>Nomor Peserta Didik</th>
        <th>Nama Peserta didik</th>
    </thead>
    <tbody>
    <?php foreach($data['pd'] as $pd): ?>
        <tr>
            <td align="right"><?=$nurut;?>.</td>
            <th><?=$pd['idxTadik'];?></th>
            <th><?=$pd['namaLengkap'];?></th>
        </tr>

    <?php $nurut++; endforeach; ?>
    </tbody>
</table>