<?php
function jampelmu($duration)
{
    list($j, $m, $d) = explode(":", $duration);
    $mj = (int)$j * 60;
    $mm = (int)$m * 1;
    $jampel = ($mj + $mm) / 45;
    return $jampel;
}

function jampele($r, $l, $j)
{
    $f = makeIntTime($r);
    $s = makeIntTime($l);
    $sejampel = $j * 60;
    return floor(($f - $s) / $sejampel);
}

function makeIntTime($t)
{
    list($h, $m, $s) = explode(":", $t);
    $timeStample = mktime($h, $m);
    return $timeStample;
}
?>
<?php $home = BASEURL . "Tutor/"; ?>

<div class="container-fluid">
    <?php Alert::sankil(); ?>
    <div class="row">
        <div class="col-lg-10">
            <?php
            if ($data['session'] != NULL) {
                $this->view('tutor/tbnav');
            }
            ?>
            <div class="card">
                <div class="card-header row">
                    <div class="col-md-10">
                        <h4>Jurnal Kegiatan :</h4>
                        <?php foreach ($data['session'] as $kelas) : ?>
                            <?= $kelas['sessionName']; ?> - [<?= "<a href='" . BASEURL . "Tutor/jourgi/" . $kelas['sessionID'] . "'>" . $kelas['sessionID'] . "</a>"; ?>]<br />
                        <?php endforeach; ?>
                    </div>
                    <div class="col-md-2">
                        <a class="btn btn-primary" data-toggle="collapse" href="#frJurnal" role="button" aria-expanded="false" aria-controls="frJurnal">
                            + Kegiatan
                        </a><br />
                        <button class="btn btn-warning" id="rapelJurnal">Jurnal Lalu</button>
                    </div>
                </div>
                <div class="card-body collapse" id="frJurnal">

                    <form action="<?= $home . "setJurnal"; ?>" method="post" class="form-horizontal">

                        <input type="hidden" name="sessionID" value="<?= $data['kelas']; ?>">

                        <div class="from-group row">
                            <label for="tanggal" class="col-md-4">Tanggal</label>
                            <div class="col-md-8">
                                <input type="date" name="tanggal" id="tanggal" class="form-control" value="<?= date('Y-m-d'); ?>" required>
                            </div>
                        </div>

                        <div class="from-group row">
                            <label for="jamMulai" class="col-md-4">Jam Mulai dan Berakhir</label>
                            <div class="col-md-8 input-group">
                                <input type="time" name="jamMulai" class="form-control" required>
                                <input type="time" name="jamAkhir" class="form-control" required>
                            </div>
                        </div>

                        <div class="from-group row">
                            <label for="kegiatan" class="col-md-4">Kegiatan</label>
                            <div class="col-md-8">
                                <input type="text" name="kegiatan" id="kegiatan" class="form-control" required maxlength='254' placeholder="maksimal 254 karakter">
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-12 d-flex justify-content-end mt-2">
                                <input type="submit" value="Simpan" class="btn btn-success">
                            </div>
                        </div>

                    </form>

                </div>
                <div class="card-body table-responsive px-0 py-0">
                    <div class="text-center">
                        <h4>Jurnal kegiatan Kelas <?= $data['nmKelas']['sessionName']; ?></h4>
                    </div>
                    <table class="table table-sm table-bordered">
                        <thead>
                            <tr class="text-center">
                                <th>Tanggal / Jam</th>
                                <th>jampel</th>
                                <th>Kegiatan</th>
                                <th>Kontrol</th>
                            </tr>
                        </thead>
                        <tbody>

                            <?php foreach ($data['jurnal'] as $jurnal) : ?>
                                <tr>
                                    <td width="300" class="text-right"><?= $jurnal['tanggal']; ?>, <?= $jurnal['jamMulai']; ?> - <?= $jurnal['jamAkhir']; ?></td>
                                    <td class="text-center"><?= jampele($jurnal['jamAkhir'], $jurnal['jamMulai'], $data['nmKelas']['mjp']); ?> jp (<?= $data['nmKelas']['mjp']; ?>)</td>
                                    <td><?= $jurnal['kegiatan']; ?></td>
                                    <td class='text-center'>
                                        <a href="javascript:void(0)" class="hapusJurnal" id="<?= $jurnal['idxJurnal']; ?>">
                                            <i class="fa fa-trash mr-2"></i>
                                        </a>
                                        <a href="javascript:void(0)" class="presensi" id="<?= $jurnal['idxJurnal']; ?>">
                                            <i class="fa fa-users mr-2"></i>
                                        </a>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-lg-2">
            <?php $this->view('tutor/tbIdentity', $data); ?>
        </div>

    </div>
</div>
<?php $this->view('template/bs4js'); ?>
<script>
    $(".hapusJurnal").click(function() {
        let idJurnal = this.id;
        let tenan = confirm("Data akan dihapus permanen!");
        if (tenan == true) {
            $.post("<?= $home . "jurnas"; ?>", {
                idxJurnal: idJurnal
            }, function(resp) {
                if (resp == '1') {
                    alert('Jurnal berhasil dihapus');
                    location.reload();
                }
            })
        }
    })

    $(".presensi").click(function() {
        window.location.href = "<?= $home . 'presensi/'; ?>" + this.id;
    })

    $('#rapelJurnal').click(function() {
        let kelas = "<?= $data['kelas']; ?>";
        window.location.href = "<?= BASEURL; ?>Tutor/massjourn/" + kelas;
    })
</script>