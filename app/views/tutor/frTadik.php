<?php $home = BASEURL."Tutor/";?>
<div class="container-fluid">
  <?php Alert::sankil(); ?>
  <div class="row">
    <div class="col-lg-10">
      <?php
      if($data['kelas'] != NULL ){
        $this->view('tutor/tbnav');
      }
      ?>
      <div class="card">
        <div class="card-header row">
            <div class="col-md-10">
                <h4>Formulir Peserta Didik Kelas :<?=$data['session']['sessionID'];?></h4>
            </div>
            <div class="col-md-2">
                &nbsp;
            </div>
        </div>
        <div class="card-body table-responsive">
            <!-- sessionID,idxTadik,namaLengkap,gender,tempatLahir,tanggalLahir -->
            <form action="<?=$home.'setGabel';?>" class="form-horizontal" method="post">
                <input type="hidden" name="mod" value="<?=$data['mod'];?>" >
                <input type="hidden" name="sessionID" value="<?=$data['kelas'];?>" >
                <div class="form-grop row">
                    <label for="idxTadik" class="col-md-4">Nomor Peserta Didik</label>
                    <div class="col-md-8">
                        <input type="text" name="idxTadik" id="idxTadik" class="form-control" readonly value="<?=$data['wb']['idxTadik'];?>" >
                    </div>
                </div>

                <div class="form-grop row">
                    <label for="namaLengkap" class="col-md-4">Nama Lengkap</label>
                    <div class="col-md-8">
                        <input type="text" name="namaLengkap" id="namaLengkap" class="form-control" value="<?=$data['wb']['namaLengkap'];?>">
                    </div>
                </div>

                <div class="form-grop row">
                    <label for="gender" class="col-md-4">Jenis Kelamin</label>
                    <div class="col-md-8">
                        <span><input type="radio" name="gender" id="tdl" value="Laki-laki">&nbsp;Laki-laki</span>
                        <span><input type="radio" name="gender" id="tdp" value="Perempuan" checked>&nbsp;Perempuan</span>
                    </div>
                </div>

                <div class="form-grop row">
                    <label for="tempatLahir" class="col-md-4">Tempat Lanir</label>
                    <div class="col-md-8">
                        <input type="text" name="tempatLahir" id="tempatLahir" class="form-control" value="<?=$data['wb']['tempatLahir'];?>" >
                    </div>
                </div>

                <div class="form-grop row">
                    <label for="tanggalLahir" class="col-md-4">Tanggal Lahir</label>
                    <div class="col-md-8">
			<?php $tm = date('m-d'); $y = date('Y') - 15; $data['wb']['tanggalLahir'] = "$y-$tm"; ?>
                        <input type="hidden" name="tanggalLahir" id="tanggalLahir" class="form-control" value="<?=$data['wb']['tanggalLahir'];?>" readonly>
                    </div>
                </div>

                <div class="form-grop row">
                    <label for="xxx" class="col-md-4">&nbsp;</label>
                    <div class="col-md-8">
                        
                        <input type="submit" class="btn btn-primary float-right" value="Simpan">
                    </div>
                </div>

            </form>
            <button class="btn btn-warning float-right" id="inputMassal">Input Massal</button>
        </div>
      </div>
    </div>
    <div class="col-lg-2">
      <?php $this->view('tutor/tbIdentity',$data);?>
    </div>
    
  </div>
</div>
<pre>
<?php
?>
</pre>

<?php $this->view('template/bs4js');?>
<script>
$("#setNewSession").click(function(){
  window.location.href="<?=BASEURL;?>Tutor/kelasBaru";
})

$('#inputMassal').click( function(){
    window.location.href='<?=BASEURL;?>Tutor/massTadik/<?=$data['kelas'];?>';
})
</script>
