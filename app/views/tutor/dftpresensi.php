<?php $home = BASEURL . "Tutor/"; ?>

<div class="container-fluid">
  <?php Alert::sankil(); ?>
  <div class="row">
    <div class="col-lg-10">
      <?php
      if ($data['session'] != NULL) {
        $this->view('tutor/tbnav');
      }
      ?>
      <div class="card">
        <div class="card-header row">
          <div class="col-md-10">
            <h4>Daftar Hadir Peserta Didik <small>Kelas:</small></h4>
            <?php foreach ($data['session'] as $kelas) : ?>
              <?= $kelas['sessionName']; ?> - [<?= "<a href='" . BASEURL . "Tutor/dhtadik/" . $kelas['sessionID'] . "'>" . $kelas['sessionID'] . "</a>"; ?>]<br />
            <?php endforeach; ?>
          </div>
          <div class="col-md-2">
            <!-- <a class="btn btn-primary" data-toggle="collapse" href="#frJurnal" role="button" aria-expanded="false" aria-controls="frJurnal">
                    + Kegiatan
                </a> -->
            &nbsp;
          </div>
        </div>
        <div class="card-body table-responsive">
          <table class="table table-sm table-striped">
            <thead>
              <tr>
                <th>Tanggal</th>
                <th>Jam</th>
                <th>Kegiatan</th>
                <th>Kontrol</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($data['dh'] as $dh) : ?>
                <tr>
                  <td><?= $dh['tanggal']; ?></td>
                  <td><?= $dh['jamMulai']; ?> - <?= $dh['jamAkhir']; ?></td>
                  <td><?= $dh['kegiatan']; ?></td>
                  <td>
                    <a href="javascript:void(0)" class="dhsession" id="<?= $dh['idxPresensi']; ?>">
                      <i class="fa fa-users icon18"></i>
                    </a>
                    <a href="javascript:void(0)" class="ml-3 gapres" id="pres_<?= $dh['idxPresensi']; ?>"><i class="fa fa-trash"></i></a>
                  </td>
                </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <div class="col-lg-2">
      <?php $this->view('tutor/tbIdentity', $data); ?>
    </div>

  </div>
</div>
<!-- Modal Siswa Hadir -->
<div class="modal" tabindex="-1" role="dialog" id="mdHadirin">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Peserta Didik Hadir</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div>Kegiatan: <span id="kegiatan"></span></div>
        <div class="table-responsive" id="hadirin"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
        <!--button type="button" class="btn btn-primary">Save changes</button-->
      </div>
    </div>
  </div>
</div>
<!-- Modal Siswa Hadir -->
<?php $this->view('template/bs4js'); ?>
<script>
  $('.dhsession').click(function() {
    let kegiatan = $(this).parent('td').parent('tr').children('td:nth-child(3)').html();
    // console.log('Keg:',kegiatan);
    $.ajax({
      url: "<?= $home; ?>hadirin/" + this.id,
      success: function(resp) {
        $("#mdHadirin").modal('show');
        $('#hadirin').html(resp);
        $('#kegiatan').html(kegiatan);
      }
    })
  })

  $(".gapres").click(function() {
    let par = this.id.split('_');
    let idxPresensi = par[1];
    let tenan = confirm("Hapus presensi?");
    if (tenan == true) {
      $.post('<?= BASEURL; ?>Tutor/gapres', {
        idxPresensi: idxPresensi
      }, function(res) {
        if (res == '1') {
          window.location.href = "<?= BASEURL; ?>Tutor/jourgi";
        }
      });
    }
  })
</script>