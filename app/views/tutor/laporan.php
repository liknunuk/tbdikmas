<?php $home = BASEURL . "Tutor/"; ?>

<div class="container-fluid">
    <?php Alert::sankil(); ?>
    <div class="row">
        <div class="col-lg-10">
            <?php
            if ($data['session'] != NULL) {
                $this->view('tutor/tbnav');
            }
            ?>
            <div class="card">
                <div class="card-header row">
                    <div class="col-md-10">
                        <h4>Laporan Bulanan Tutor Bantu
                        </h4>
                    </div>
                    <div class="col-md-2">
                        <a class="btn btn-primary" data-toggle="collapse" href="#frLaporan" role="button" aria-expanded="false" aria-controls="frJurnal">
                            + Laporan
                        </a>
                        &nbsp;
                    </div>
                </div>
                <div class="card-body">
                    <?php if ($data['idlap'] == "") : ?>
                        <div id="frLaporan" class="collapse">
                        <?php else : ?>
                            <div id="frLaporan">
                            <?php endif; ?>

                            <form action="<?= $home; ?>setLaporan" method="post" class="form-horizontal">
                                <?php
                                if ($data['idlap'] == "") {
                                    $mod = "baru";
                                    $idxLap = "";
                                } else {
                                    $mod = "ubah";
                                    $idxLap = $data['idlap'];
                                }

                                ?>
                                <input type="hidden" name="tbId" value="<?= $_SESSION['tb']; ?>">
                                <input type="hidden" name="mod" value="<?= $mod; ?>">
                                <input type="hidden" name="idxLaporan" value="<?= $idxLap; ?>">
                                <div class="form-group row">
                                    <label for="tipelap" class="col-md-4">Jenis Laporan</label>
                                    <div class="col-md-8">
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="tipelap" id="tipelapa" value="akhir">
                                            <label class="form-check-label" for="tipelapa">Lap. Akhir</label>
                                        </div>

                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" checked name="tipelap" id="tipelapb" value="bulanan">
                                            <label class="form-check-label" for="tipelapb">Lap. Bulanan</label>
                                        </div>
                                    </div>
                                </div>

                                <?php
                                $tahun = date('Y');
                                $bulan = array(
                                    '01' => 'Januari',
                                    '02' => 'Februari',
                                    '03' => 'Maret',
                                    '04' => 'April',
                                    '05' => 'Mei',
                                    '06' => 'Juni',
                                    '07' => 'Juli',
                                    '08' => 'Agustus',
                                    '09' => 'September',
                                    '10' => 'Oktober',
                                    '11' => 'Nopember',
                                    '12' => 'Desember'
                                );
                                ?>

                                <div class="form-group row">
                                    <label for="bulan" class="col-md-4">Bulan Laporan</label>
                                    <div class="col-md-8">
                                        <select name="bulan" id="bulan" class="form-control">
                                            <?php
                                            $thlalu = date('Y') - 1;
                                            echo "<option value='" . $thlalu . "-12'>Desember {$thlalu}</option>";
                                            foreach ($bulan as $ak => $nb) :
                                            ?>
                                                <?php
                                                if ($data['idlap'] == "") {
                                                    if ($tahun . '-' . $ak == date('Y-m')) {
                                                        $sel = 'selected';
                                                    } else {
                                                        $sel = "";
                                                    }
                                                } else {
                                                    if ($tahun . '-' . $ak == $data['tblap']['bulan']) {
                                                        $sel = 'selected';
                                                    } else {
                                                        $sel = "";
                                                    }
                                                }
                                                ?>
                                                <option <?= $sel; ?> value="<?= $tahun . '-' . $ak; ?>"><?= $nb . ' ' . $tahun; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="tanggal" class="col-md-4">tanggal</label>
                                    <div class="col-md-8">
                                        <input type="date" name="tanggal" id="tanggal" class="form-control" value="<?= $data['tblap']['tanggal']; ?>">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="dgLink" class="col-md-4">Link Drive Google</label>
                                    <div class="col-md-8">
                                        <input type="text" name="dgLink" id="dgLink" class="form-control" value="<?= $data['tblap']['dgLink']; ?>">
                                    </div>
                                </div>

                                <div class="d-flex justify-content-end mt-3">
                                    <button type="submit" class="btn btn-primary">Kirim Laporan</button>
                                </div>

                            </form>
                            </div>
                        </div>
                        <div class="card-body table-responsive">
                            <table class="table table-sm table-striped">
                                <thead>
                                    <tr>
                                        <th>Jenis</th>
                                        <th>Bulan</th>
                                        <th>Tanggal Upload</th>
                                        <th>Link Google Drive</th>
                                        <th>Kontrol</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($data['laporan'] as $laporan) : ?>

                                        <tr>
                                            <td><?= $laporan['tipelap']; ?></td>
                                            <td><?= $laporan['bulan']; ?></td>
                                            <td><?= $laporan['tanggal']; ?></td>
                                            <td>
                                                <?php
                                                $txt = explode('/', $laporan['dgLink']);
                                                ?>
                                                <a href="<?= $laporan['dgLink']; ?>" target="_blank"><?= $txt[5]; ?></a>
                                            </td>
                                            <td>
                                                <a href="javascript:void(0)" class="laprmv" id="r-<?= $laporan['idxLaporan']; ?>"><i class="fa fa-trash"></i></a>
                                                <a href="javascript:void(0)" class="lapchg" id="c-<?= $laporan['idxLaporan']; ?>"><i class="fa fa-edit"></i></a>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                </div>
            </div>
            <div class="col-lg-2">
                <?php $this->view('tutor/tbIdentity', $data); ?>
            </div>

        </div>
    </div>

    <?php $this->view('template/bs4js'); ?>
    <script>
        $(".lapchg").click(function() {
            let idl = $(this).prop('id');
            idl = idl.split('-');
            let id = idl[1];
            window.location.href = "<?= BASEURL; ?>Tutor/lapor/" + id;
        })

        $(".laprmv").click(function() {
            let idl = $(this).prop('id');
            idl = idl.split('-');
            let id = idl[1];

            let tenan = confirm("Laporan " + id + " akan dihapus!");
            if (tenan == true) {

                $.post("<?= BASEURL; ?>Tutor/cabutLaporan", {
                        id: id
                    },
                    function(resp) {
                        location.reload();
                    }
                );
            }

        })
    </script>