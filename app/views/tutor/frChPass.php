<div class="container-fluid">
    <div class="row mt-5">
        <div class="col-md-4 mx-auto">
            <h4>Ganti Password</h4>
            <form action="<?=BASEURL?>Tutor/kuncibaru" method="post">
                <div class="form-group row">
                    <label for="nomor" class="col-sm-3">Nm. urut</label>
                    <div class="col-sm-9">
                        <input type="text" name="ktb" id="nomor" class="form-control" readonly value="<?=$data['ktb'];?>">
                    </div>
                </div>
                
                <div class="form-group row">
                    <label for="tbUsername" class="col-sm-3">Email</label>
                    <div class="col-sm-9">
                        <input type="email" name="usr" id="tbUsername" class="form-control">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="tbPassword" class="col-sm-3">Password</label>
                    <div class="col-sm-9">
                        <input type="password" name="psw" id="tbPassword" class="form-control">
                        <span id="pwmu"></span>
                    </div>
                </div>

                <div class="form-group d-flex justify-content-end">
                    <input type="submit" value="Update" class="btn btn-primary">
                </div>

            </form>
        </div>
    </div>
</div>
<?php $this->view('template/bs4js'); ?>
<script>
$('#tbPassword').keyup( function(){

    $('#pwmu').css( "font-family","monospace" );
    $('#pwmu').text( $(this).val());
})
</script>