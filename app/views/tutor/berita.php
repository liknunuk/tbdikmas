<div class="container-fluid">
  <?php Alert::sankil(); ?>
  <div class="row">
    <div class="col-lg-10">
      <?php
      if($data['session'] != NULL ){
        $this->view('tutor/tbnav');
      }
      ?>
      <div class="card">
        <div class="card-header">
          <h4>Informasi Tutor Bantu</h4>
        </div>
        <div class="card-body">
          <article>
              <span><small><em><?=$data['berita']['author'];?> - <?=$data['berita']['waktu'];?></em></small></span>
              <h4><?=$data['berita']['judul'];?></h4>
              <?=$data['berita']['berita'];?>
          </article>
          
        </div>
      </div>
    </div>
    <div class="col-lg-2">
      <?php $this->view('tutor/tbIdentity',$data);?>
    </div>
    
  </div>
</div>
<pre>
<?php
?>
</pre>

<?php $this->view('template/bs4js');?>
<script>
$("#setNewSession").click(function(){
  window.location.href="<?=BASEURL;?>Tutor/kelasBaru";
})
</script>