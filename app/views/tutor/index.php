<div class="container-fluid">
  <div class="row">
    <div class="col">
      <?php $this->view('tutor/tbnav'); ?>
    </div>
  </div>
  <div class="row">
    <div class="col-lg-10">
      <h4>UPDATE INFORMASI TUTOR BANTU</h4>
      <div class="list-group">
        <?php foreach ($data['berita'] as $berita) : ?>
          <li class="list-group-item">
            <?= $berita['judul']; ?><br>
            <?= $berita['waktu']; ?>
          </li>
        <?php endforeach; ?>
      </div>
    </div>
    <div class="col-lg-2">
      <?php $this->view('tutor/tbIdentity', $data); ?>
      <div class="d-flex justify-content-center mt-3">
        <a href="<?= BASEURL; ?>Tutor/kelasBaru" class="btn btn-primary">+ Kelas</a>
      </div>
    </div>
  </div>
</div>
<?php $this->view('template/bs4js'); ?>
