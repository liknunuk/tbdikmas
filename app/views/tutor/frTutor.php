<div class="container-fluid mt-3">
    <div class="row d-flex justify-content-center">
        <div class="col-lg-6">
            <h3 class='text-center'>PROFIL TUTOR</h3>
        </div>
    </div>
    <div class="row d-flex justify-content-center">
        <div class="col-lg-6">

         <form action="<?=BASEURL;?>Tutor/SetIdtty" method="post">
            <input type="hidden" name="nomor" value="<?=$data['tb']['nomor'];?>">
            <div class="form-group">
                <label for="ktb">Nomor Tutor</label>
                <input type="text" name="ktb" class="form-control" value="<?=$data['tb']['ktb'];?>" disabled >
            </div>

            <div class="form-group">
                <label for="namaPenerima">Nama Tutor</label>
                <input type="text" name="namaPenerima" class="form-control" value="<?=$data['tb']['namaPenerima'];?>">
            </div>

            <div class="form-group">
                <label for="kota">Dari SKB Kota </label>
                <input type="text" name="kota" class="form-control" value="<?=$data['tb']['kota'];?>">
            </div>

            <div class="form-group">
                <label for="program">Dari SKB program </label>
                <select name="program" class="form-control">
                    <option value="<?=$data['tb']['program'];?>"><?=$data['tb']['program'];?></option>
                    <option value="KEAKSARAAN">Keaksaraan</option>
                    <option value="KESETARAAN">Kesetaraan</option>
                    <option value="KURSUS">Keterampilan Fungsional</option>
                    <option value="PAUD">PAUD</option>
                </select>
            </div>

            <div class="form-group">
                <label for="kodeNPWP">Kode NPWP </label>
                <input type="text" name="kodeNPWP" class="form-control" value="<?=$data['tb']['kodeNPWP'];?>">
            </div>

            <div class="form-group">
                <label for="nomorRekening">Nomor Rekening BRI </label>
                <input type="text" name="nomorRekening" class="form-control" value="<?=$data['tb']['nomorRekening'];?>">
                <input type="text" disabled class='form-control' value="<?=$data['tb']['namaBank'];?>">
            </div>

            <div class="form-group text-right">
                <a href="<?=BASEURL;?>Tutor" class="btn btn-warning">Batal</a>
                <button type="submit" class="btn btn-primary">Update</button>
            </div>
         </form>
        </div>
    </div>
</div>