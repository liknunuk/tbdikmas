<?php
// Membatasi TB agar cuma punya 1 kelas virtual
// if($data['session']['sessionID'] !== NULL ){
//     header("Location:".BASEURL."Tutor");
//     exit();
// }
?>
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <h3>Kelas Tutor Bantu</h3>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <form action="<?= BASEURL; ?>Tutor/setSession/<?= $data['mod']; ?>" method="post" class="form-horizontal">

                <input type="hidden" name="tbid" value="<?= sprintf('%03d', $_SESSION['tb']); ?>">
                <div class="form-group row">
                    <label for="sessionID" class="col-sm-4">ID Kelas</label>
                    <div class="col-sm-8">
                        <input type="text" name="sessionID" id="sessionID" class="form-control" value="<?= $data['sessionID']; ?>" readonly>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="sessionName" class="col-sm-4">Nama Kelas</label>
                    <div class="col-sm-8">
                        <input type="text" name="sessionName" id="sessionName" class="form-control" required>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-sm-4" for="periodeBegin">Durasi 1 jampel</label>
                    <div class="col-sm-8">
                        <select name="mjp" id="mjp" class="form-control">
                            <option value="35">35 Menit</option>
                            <option value="45">45 Menit</option>
                            <option value="60">60 Menit</option>
                        </select>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="periodeBegin" class="col-sm-4">Tanggal Mulai</label>
                    <div class="col-sm-8">
                        <input type="date" name="periodeBegin" id="periodeBegin" class="form-control" required>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="periodeEnded" class="col-sm-4">Tanggal Berakhir</label>
                    <div class="col-sm-8">
                        <input type="date" name="periodeEnded" id="periodeEnded" class="form-control" required>
                    </div>
                </div>

                <div class="form-group">
                    <input type="submit" value="Simpan" class="btn btn-primary float-right">
                </div>

            </form>
        </div>
    </div>
</div>

<pre>
<?php
// print_r($data);
?>
</pre>