<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PESERTA DIDIK <?=$data['kelas'];?></title>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script src="https://bossanova.uk/jspreadsheet/v4/jexcel.js"></script>
    <link rel="stylesheet" href="https://bossanova.uk/jspreadsheet/v4/jexcel.css" type="text/css" />
    <script src="https://jsuites.net/v4/jsuites.js"></script>
    <link rel="stylesheet" href="https://jsuites.net/v4/jsuites.css" type="text/css" />
</head>
<body>
    <h4>Data Peserta Didik</h4>
    <p>Silakan tempel / sematkan data peserta didik dari file excel / spreadsheet dengan urutan kolom yang sama</p>
    <div id="spreadsheet"></div>
    <div>
        <button id="setData">Simpan</button>
    </div>
    <div id="tblog"></div>
    <script>
    var options = {
        data: [],
        columns: [
            { type:'text', width:100, title:'Urut' , align:'left' },
            { type:'text', width:400, title:'Nama' , align:'left' },
            { type:'dropdown', width:100, title:'Jenis Kelamin',source:['Laki-laki','Perempuan'] },
            { type:'text', width:200, title:'Tempat Lahir', align: 'center' },
        ],
        //  style: {
        //     A1:'text-align:center; font-weight:bold;',
        //     B1:'background-color: orange;',
        // },
        minDimensions:[4,10],
    }
        var table1 = jspreadsheet(document.getElementById('spreadsheet'), options);
            
        $('#setData').click(function(){
            // console.log ( table1.getJson(false) );
            let tbdata = table1.getData(false);
            // $('#tblog').html(tbdata[0][0]);
            $.post('<?=BASEURL;?>Tutor/setMassStudents/',{
                data:tbdata,
                sessionID:'<?=$data['kelas'];?>',
                tbID : '<?=$_SESSION['tb']; ?>'
                },function(res){
                if(res == '1'){
                    window.location.href='<?=BASEURL;?>Tutor/tadik/<?=$data['kelas'];?>';
                }
            })
        })
    </script>
</body>
</html>