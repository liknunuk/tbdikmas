<?php
$kelasAktif = $data['session'] == NULL ? "Belum ada. <a href='javascript:void(0)' id='setNewSession'>Buat Baru</a>" : $data['session']['sessionID'];
?>
<div class="list-group mt-3">
    <li class="list-group-item font-weight-bold py-0">Nama Tutor Bantu</li>
    <li class="list-group-item py-0"><?= $_SESSION['namaPenerima']; ?></li>
    <li class="list-group-item font-weight-bold py-0">Program</li>
    <li class="list-group-item py-0"><?= $_SESSION['program']; ?></li>
    <li class="list-group-item font-weight-bold py-0">SKB Kab./Kota</li>
    <li class="list-group-item py-0"><?= $_SESSION['kota']; ?></li>
    <li class="list-group-item font-weight-bold py-0">Kelas Aktif</li>

    <?php
    if ($data['session'] != NULL) :
        foreach ($data['session'] as $kelas) : ?>
            <li class="list-group-item py-0"><a href="<?= BASEURL; ?>Tutor/rombel/<?= $kelas['sessionID']; ?>"><?= $kelas['sessionID']; ?></a></li>
    <?php
        endforeach;
    endif;
    ?>

    <li class="list-group-item py-0">
        <a href="<?= BASEURL; ?>Tutor/chgIdtty">Edit Profil</a>
    </li>
    <li class="list-group-item py-0">
        <a href="<?= BASEURL ?>Tutor/sulihasma">Ganti Password</a>
    </li>
</div>