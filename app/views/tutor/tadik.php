<?php $home = BASEURL."Tutor/";?>
<div class="container-fluid">
  <?php Alert::sankil(); ?>
  <div class="row">
    <div class="col-lg-10">
      <?php
      if($data['session'] != NULL ){
        $this->view('tutor/tbnav');
      }
      ?>
      <div class="card">
        <div class="card-header row">
            <div class="col-md-10">
                <h4>Peserta Didik Kelas :</h4>
                <?php foreach($data['session'] as $kelas):?>
                <?=$kelas['sessionName'];?> - [<?="<a href='".BASEURL."Tutor/tadik/".$kelas['sessionID']."'>".$kelas['sessionID']."</a>";?>]<br/>
                <?php endforeach; ?>
            </div>
            <div class="col-md-2">
                <a href="<?=$home."frTadik/".$data['kelas']."/baru";?>" class="btn btn-primary">
                    + Peserta Didik
                </a>
            </div>
        </div>
        <div class="card-body table-responsive px-0 py-0">
            <?php if($data['kelas'] == NULL ): ?>
            <h4 class="bg-danger text-warning text-center py-3">Pilih Kelas Dahulu!</h4>
            <?php endif; ?>
            <table class="table table-sm table-bordered">
            <!-- idxTadik,namaLengkap,gender,tempatLahir,tanggalLahir -->
                <thead>
                    <tr>
                        <th>Nomor</th>
                        <th>Nama Lengkap</th>
                        <th>Jn. Kelamin</th>
                        <th>Tempat dan Tanggal Lahir</th>
                        <th>Kontrol</th>
                    </tr>
                </thead>
                <tbody>
                <?php 
                if($data['tadik'] != NULL ){

                
                  foreach($data['tadik'] as $tadik ): ?>
                      <tr>
                          <td><?=$tadik['idxTadik'];?></td>
                          <td><?=$tadik['namaLengkap'];?></td>
                          <td><?=$tadik['gender'];?></td>
                          <td><?=$tadik['tempatLahir'];?>, <?php //$tadik['tanggalLahir'];?></td>
                          <td class='text-center'>
                              <a href="javascript:void(0)" class='tadikMutasi' id="<?=$tadik['idxTadik'];?>">
                                <i class="fa fa-user-times mr-2"></i>
                              </a>
                              <a href="<?=$home?>frTadik/<?=$data['kelas']?>/lama/<?=$tadik['idxTadik'];?>">
                                <i class="fa fa-edit"></i>
                              </a>
                          </td>
                      </tr>
                <?php 
                  endforeach; 
                }
                  
                  ?>
                </tbody>
            </table>
            <div class="text-center">
              <button class="btn btn-primary"><a class='text-light' href="<?=BASEURL.'Tutor/tadik/'.$data['kelas'].'/'.$data['pp'];?>">Sebelum</a></button>
              <button class="btn btn-primary"><a class='text-light' href="<?=BASEURL.'Tutor/tadik/'.$data['kelas'].'/'.$data['np'];?>">Berikut</a></button>
            </div>
        </div>
      </div>
    </div>
    <div class="col-lg-2">
      <?php $this->view('tutor/tbIdentity',$data);?>
    </div>
    
  </div>
</div>
<pre>
<?php
?>
</pre>

<?php $this->view('template/bs4js');?>
<script>
$("#setNewSession").click(function(){
  window.location.href="<?=BASEURL;?>Tutor/kelasBaru";
})

$(".tadikMutasi").click(function(){
  let tenan = confirm('Peserta didik akan dihapus!');
  if( tenan == true ){
    $.post('<?=$home;?>tadikMutasi',{
      idxTadik: this.id
    },function(){
      location.reload();
    })
  }
})
</script>
