<div class="container-fluid">
  <?php Alert::sankil(); ?>
  <div class="row">
    <div class="col-lg-10">
      <?php
      if ($data['session'] != NULL) {
        $this->view('tutor/tbnav');
      }
      ?>
      <div class="card">
        <div class="card-header">
          <h4>Informasi Rombel / Kelas Aktif</h4>
          <?php # print_r($data['sesinfo']);
          ?>
          <table class="table table-sm">

            <tbody>
              <tr>
                <td>Kode Rombel / Kelas</td>
                <td width="50%"><?= $data['sesinfo']['sessionID']; ?></td>
              </tr>
              <tr>
                <td>Nama Rombel / Kelas</td>
                <td><?= $data['sesinfo']['sessionName']; ?></td>
              </tr>
              <tr>
                <td>Durasi 1 jampel</td>
                <td><?= $data['sesinfo']['mjp']; ?> Menit</td>
              </tr>
              <tr>
                <td>Mulai Tanggal</td>
                <td><?= $data['sesinfo']['periodeBegin']; ?></td>
              </tr>
              <tr>
                <td>Hingga Tanggal</td>
                <td><?= $data['sesinfo']['periodeEnded']; ?></td>
              </tr>
              <tr>
                <td>Kontrol</td>
                <td>
                  <button class="btn btn-danger" id="kukud">
                    <i class="fa fa-trash" style="font-size:24px">&nbsp; Hapus</i>
                  </button>
                  &nbsp;
                  <button class="btn btn-primary" data-toggle="modal" data-target="#modalRombel">
                    <i class="fa fa-pencil-square" style="font-size:24px">&nbsp; Edit</i>
                  </button>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
        <div class="card-body">

        </div>
      </div>
    </div>
    <div class="col-lg-2">
      <?php $this->view('tutor/tbIdentity', $data); ?>
      <div class="d-flex justify-content-center mt-3">
        <a href="<?= BASEURL; ?>Tutor/kelasBaru" class="btn btn-primary">+ Kelas</a>
      </div>

    </div>

  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="modalRombel" tabindex="-1" role="dialog" aria-labelledby="labelModalRombel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="labelModalRombel">DATA ROMBEL</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="<?= BASEURL; ?>Tutor/chgRombel" method="post">
          <div class="form-group">
            <label for="sessionId">Kode Rombel</label>
            <input type="text" name="sessionID" id="sessionID" class="form-control" readonly value="<?= $data['sesinfo']['sessionID']; ?>">
          </div>

          <div class="form-group">
            <label for="sessionName">Nama Rombel</label>
            <input type="text" name="sessionName" id="sessionName" class="form-control" value="<?= $data['sesinfo']['sessionName']; ?>">
          </div>

          <div class="form-group">
            <label for="periodeBegin">Durasi 1 jampel</label>
            <select name="mjp" id="mjp" class="form-control">
              <option value="35" <?= $data['sesinfo']['mjp'] == 35 ? 'selected' : ''; ?>>35 Menit</option>
              <option value="45" <?= $data['sesinfo']['mjp'] == 45 ? 'selected' : ''; ?>>45 Menit</option>
              <option value="60" <?= $data['sesinfo']['mjp'] == 60 ? 'selected' : ''; ?>>60 Menit</option>
            </select>
          </div>

          <div class="form-group">
            <label for="periodeBegin">Mulai aktif</label>
            <input type="date" name="periodeBegin" id="periodeBegin" class="form-control" value="<?= $data['sesinfo']['periodeBegin']; ?>">
          </div>

          <div class="form-group">
            <label for="periodeEnded">Berakhir pada</label>
            <input type="date" name="periodeEnded" id="periodeEnded" class="form-control" value="<?= $data['sesinfo']['periodeEnded']; ?>">
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
        </form>
      </div>
    </div>
  </div>
</div>
<?php $this->view('template/bs4js'); ?>
<script>
  $("#setNewSession").click(function() {
    window.location.href = "<?= BASEURL; ?>Tutor/kelasBaru";
  })

  $('#kukud').click(function() {
    let rombel = '<?= $data['sesinfo']['sessionID']; ?>';
    let tenan = confirm('Rombel akan dihapus!');
    if (tenan == true) {
      $.post('<?= BASEURL; ?>Tutor/remrombel', {
        sessionID: rombel
      }, function(res) {
        console.log(res);
        if (res == '1') {
          window.location.href = '<?= BASEURL; ?>Tutor';
        }
      })
    }
  })
</script>