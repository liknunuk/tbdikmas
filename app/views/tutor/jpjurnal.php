<?php
$namaBulan = [
    '01' => 'Januari',
    '02' => 'Februari',
    '03' => 'Maret',
    '04' => 'April',
    '05' => 'Mei',
    '06' => 'Juni',
    '07' => 'Juli',
    '08' => 'Agustus',
    '09' => 'September',
    '10' => 'Oktober',
    '11' => 'Nopember',
    '12' => 'Desember'
];


?>
<div class="container">
    <div class="row">
        <div class="col-lg-6 mx-auto mt-3">
            <h2 class="text-center">INPUT JAM PELAJARAN</h2>
            <div class="row">
                <div class="col-sm-4 mx-auto">
                    <select id="bulap" class="form-control">
                        <option value="">Pilih Bulan Laporan</option>
                        <?php foreach ($namaBulan as $nomor => $nama) : ?>
                            <option value="<?= date('Y'); ?>-<?= $nomor; ?>"><?= $nama . ' ' . date('Y'); ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>

        </div>
    </div>
</div>
<?= $this->view('template/bs4js'); ?>