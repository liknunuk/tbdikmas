<?php $home=BASEURL."Tutor/";?>
<nav class="navbar navbar-expand-lg navbar-dark" style="background-color: maroon;">
  <a class="navbar-brand" href="<?=$home;?>">TUTOR BANTU JATENG</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <a class="nav-link" href="<?=$home."tadik";?>">PESERTA DIDIK</a>
      </li>

      <li class="nav-item">
        <a class="nav-link" href="<?=$home."jourgi";?>">JURNAL KEGIATAN</a>
      </li>

      <li class="nav-item">
        <a class="nav-link" href="<?=$home."dhtadik";?>">PRESENSI</a>
      </li>

      <li class="nav-item">
        <a class="nav-link" href="<?=$home."lapor";?>">LAPORAN</a>
      </li>

      <li class="nav-item">
        <a class="nav-link" href="<?=$home."liquidasi";?>">TRANSFER</a>
      </li>

    </ul>
    <ul class="navbar-nav my-2 my-lg-0">
      <li class="nav-item">
        <a class="nav-link" href="<?=BASEURL;?>Home/logout">LOGOUT</a>
      </li>
    </ul>
  </div>
</nav>