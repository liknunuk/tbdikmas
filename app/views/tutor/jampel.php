<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <h2>INPUT JAM PELAJARAN BULANAN</h2>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <?php Alert::sankil(); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6">
            <form action="<?= BASEURL; ?>Jp/setJampel" method="post" class="form-horizontal">
                <!-- Program -->
                <div class="form-group row">
                    <label for="program" class="col-md-4">Program</label>
                    <div class="col-md-8">
                        <select name="program" id="program" class="form-control">
                            <option value="">Pilih Program</option>
                            <option value="TS">Kesetaran</option>
                            <option value="TP">PAUD</option>
                        </select>
                    </div>
                </div>

                <!-- Nomor Urut -->
                <div class="form-group row">
                    <label for="nmUrut" class="col-md-4">Nomor Urut</label>
                    <div class="col-md-4">
                        <input type="number" name="nmUrut" id="nmUrut" class="form-control" min=1 max=152>
                    </div>

                    <div class="col-md-4">
                        <a id="cekTb" class="btn btn-secondary">Cek Data TB</a>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="bulan" class="col-md-4">Bulan</label>
                    <div class="col-md-8">
                        <select name="bulan" id="bulan" class="form-control">
                            <option value="">Pilih Bulan</option>
                            <?php
                            $tahun = date('Y');
                            $bulan = [
                                "01" => "Januari",
                                "02" => "Februari",
                                "03" => "Maret",
                                "04" => "April",
                                "05" => "Mei",
                                "06" => "Juni",
                                "07" => "Juli",
                                "08" => "Agustus",
                                "09" => "September",
                                "10" => "Oktober",
                                "11" => "Nopember",
                                "12" => "Desember"
                            ];

                            foreach ($bulan as $ank => $nmb) :
                            ?>

                                <option value="<?= $tahun . "-" . $ank; ?>"><?= $nmb . " " . $tahun; ?></option>

                            <?php endforeach; ?>

                        </select>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="jampel" class="col-md-4">Total Jam Pelajaran<br><small>Sesuai jurnal</small></label>
                    <div class="col-md-8">
                        <input type="number" name="jampel" id="jampel" class="form-control" min=1 max=300>
                    </div>
                </div>

                <div class="form-group row d-flex justify-content-end">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>

            </form>
        </div>

        <div class="col-lg-6">
            <h4>Identitas Tutor</h4>

            <div id="tutorData"></div>
        </div>


    </div>
</div>
<?php $this->view('template/bs4js'); ?>
<script>
    $("#cekTb").click(function() {
        let prg, nmu, nm3, ktb;
        prg = $('#program').val();
        nmu = $('#nmUrut').val();
        nm3 = parseInt(nmu).pad(3);
        ktb = prg + nm3;
        // alert(ktb)
        $("#tutorData").html('');
        $.ajax({
            url: "<?= BASEURL; ?>Jp/tbData/" + ktb,
            success: function(resp) {
                $("#tutorData").html(resp);
            }
        });
    })

    $('#bulan').change(function() {
        let noTutor, blLapor;
        noTutor = $('#nmUrut').val();
        blLapor = $(this).val();
        $.ajax({
            url: '<?= BASEURL; ?>Jp/cekJpJurnal/' + noTutor + '/' + blLapor,
            success: function(resp) {
                if (resp != "0") {
                    $('#jampel').val(resp);
                    $('#jampel').prop('readonly', false);
                } else {
                    $('#jampel').val('0');
                    $('#jampel').prop('readonly', false);
                }
            }
        })
    })

    // leading zero generator
    Number.prototype.pad = function(size) {
        var s = String(this);
        while (s.length < (size || 2)) {
            s = "0" + s;
        }
        return s;
    }
</script>