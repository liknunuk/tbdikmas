<?php $home = BASEURL."Tutor/";?>
<div class="container-fluid">
  <?php Alert::sankil(); ?>
  <div class="row">
    <div class="col-lg-10">
      <?php
      if($data['session']['sessionID'] != NULL ){
        $this->view('tutor/tbnav');
      }
      ?>
      <div class="card">
        <div class="card-header row">
            <div class="col-md-10">
                <h4>Formulir Peserta Didik Kelas :<?=$data['session']['sessionID'];?></h4>
            </div>
            <div class="col-md-2">
                &nbsp;
            </div>
        </div>
        <div class="card-body table-responsive">
            <!-- sessionID,idxTadik,namaLengkap,gender,tempatLahir,tanggalLahir -->
            <form action="<?=$home.'setGabel';?>" class="form-horizontal" method="post">
                <input type="hidden" name="mod" value="<?=$data['mod'];?>" >
                <input type="hidden" name="sessionID" value="<?=$data['session']['sessionID'];?>" >
                
                <div class="form-group row">
                    <label for="tanggal" class="col-md-4">Tanggal</label>
                    <div class="col-md-8">
                        <input type="text" name="tanggal" id="tanggal" class="form-control">
                    </div>
                </div>
                
                <div class="form-group row">
                    <label for="xxx" class="col-md-4">xxx</label>
                    <div class="col-md-8">
                        <input type="text" name="xxx" id="xxx" class="form-control">
                    </div>
                </div>
                
                <div class="form-group row">
                    <label for="xxx" class="col-md-4">xxx</label>
                    <div class="col-md-8">
                        <input type="text" name="xxx" id="xxx" class="form-control">
                    </div>
                </div>

                <div class="form-grop row">
                    <label for="xxx" class="col-md-4">&nbsp;</label>
                    <div class="col-md-8">
                        <input type="submit" class="btn btn-primary float-right" value="Simpan">
                    </div>
                </div>

            </form>
        </div>
      </div>
    </div>
    <div class="col-lg-2">
      <?php $this->view('tutor/tbIdentity',$data);?>
    </div>
    
  </div>
</div>
<pre>
<?php
?>
</pre>

<?php $this->view('template/bs4js');?>
<script>
$("#setNewSession").click(function(){
  window.location.href="<?=BASEURL;?>Tutor/kelasBaru";
})
</script>