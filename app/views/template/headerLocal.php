<!DOCTYPE html>
<html lang="en">
<head>
  <!-- Theme Made By www.w3schools.com - No Copyright -->
  <title><?= $data['title']; ?></title>
  <meta charset="utf-8">
  <link rel="shortcut icon" href="<?=BASEURL;?>/img/logo-idi.png" width="16px" type="image/png">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- bootstrap 4 CDN -->
  <link rel="stylesheet" href="http://likgroho.mugeno.org/bs4/css/bootstrap.min.css">
  <!-- jquery-ui -->
  <link rel="stylesheet" href="http://likgroho.mugeno.org/bs4/css/jquery-ui.css">
  <!-- font awesome -->
  <link rel="stylesheet" href="http://likgroho.mugeno.org/bs4/css/font-awesome.min.css">
  <!-- Custom style -->
  <link rel="stylesheet" href="<?= BASEURL .'/css/p2paud.css';?>" />
  <!-- Custom Script u/ restful API -->
  <!--script> var pusdata = "< ?=PUSDATA;?>"; var baseurl = "< ?=BASEURL;?>"; </script-->
</head>
<body>
<div class="container-fluid">
  <div class="row">
    <div class="col-lg-12">
    <h4>PUSAT PENGEMBANGAN PENDIDIKAN ANAK USIA DINI<br>
    DAN PENDIDIKAN MASYARAKAT JAWA TENGAH</h4>
    <hr class="hr-bold">
    </div>
  </div>
</div>
