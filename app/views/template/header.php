<?php
if(!isset($_SESSION['admin'])){
  header("Location:" . BASEURL . "P2paud/login");
  exit();
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <!-- Theme Made By www.w3schools.com - No Copyright -->
  <title><?= $data['title']; ?></title>
  <meta charset="utf-8">
  <link rel="shortcut icon" href="https://awsimages.detik.net.id/community/media/visual/2019/09/05/f2399a3c-e44e-4e2a-82d0-520696ffad1b.png" width="16px" type="image/png">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- bootstrap 4 CDN -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <!-- jquery-ui -->
  <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <!-- font awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <!-- Custom style -->
  <link rel="stylesheet" href="<?= BASEURL .'css/p2paud.css';?>" />
  <!-- Custom Script u/ restful API -->
  <!--script> var pusdata = "< ?=PUSDATA;?>"; var baseurl = "< ?=BASEURL;?>"; </script-->
  <!-- tinymce -->
  <style>
    html,body{width: 100%; height: 100%;}
    #banner{
      background-image: url("<?=BASEURL;?>/img/p2paud-header.png");
      background-repeat: no-repeat;
      background-attachment: fixed;
    }
    #banner h4{
      color: orange; font-weight: bold;
      text-shadow: 2px 2px 0px navy;
    }

    li.list-header{
      background: linear-gradient( to right, brown, maroon, brown);
      padding: 5px 15px;
      border-radius: 10px;
      box-sizing: border-box;
    }
    li.list-header a { color: yellow; text-decoration: none;}
    li.list-header a:hover { font-weight: bold; }
  </style>
</head>
<body>
<div class="container-fluid">
  <div class="row">
    <div class="col-lg-12 py-3" id="banner">
    <h4>PUSAT PENGEMBANGAN PENDIDIKAN ANAK USIA DINI<br>
    DAN PENDIDIKAN MASYARAKAT JAWA TENGAH</h4>
    <ul class="list-inline">
      <li class="list-inline-item list-header"><a href="<?=BASEURL;?>P2paud">Beranda</a></li>
      <li class="list-inline-item list-header"><a href="<?=BASEURL;?>P2paud/tblabul">Laporan TB</a></li>
      <li class="list-inline-item list-header"><a href="<?=BASEURL;?>P2paud/transfer">Transfer</a></li>
      <li class="list-inline-item list-header"><a href="<?=BASEURL;?>P2paud/buktiTransfer">Bukti Transfer</a></li>
      <li class="list-inline-item list-header"><a href="<?=BASEURL;?>P2paud/tbinfo">Informasi</a></li>
      <li class="list-inline-item list-header"><a href="<?=BASEURL;?>P2paud/logout"><i class="fa fa-sign-out"></i></a></li>
    </ul>
    </div>
  </div>
</div>

