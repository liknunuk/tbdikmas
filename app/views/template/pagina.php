<?php
    if($data['pn'] == 1){
        $next = $data['pn'] + 1;
        $prev = 1;
    }else{
        $next = $data['pn'] + 1;
        $prev = $data['pn'] - 1;
    }
?>
<div class='row'>
    <div class='col-md-12'>
        <nav aria-label="pageNav" class='float-right mx-3'>
            <ul class="pagination">
                <li class="page-item"><a class="page-link" href="<?=BASEURL.$data['method'].'/'.$prev;?>">&lt;&lt;</a></li>
                <li class="page-item"><a class="page-link" href="#"><?=$data['pn'];?></a></li>
                <li class="page-item"><a class="page-link" href="<?=BASEURL.$data['method'].'/'.$next;?>">&gt;&gt;</a></li>
            </ul>
        </nav>
    </div>
</div>