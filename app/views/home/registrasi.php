<div class="container mt-2">
    <div class="row mb-3">
        <div class="col-lg-12">
            <h4 class='text-danger'>Penting!</h4>
            <p class="text-justify" style="text-indent:25px;">Halaman ini ditujukan untuk mengakomodasi rekan-rekan Tutor Bantu Jawa Tengah memperoleh username dan kata sandi pada sistem TBASSISTANT. Username dan kata sandi tersebut tidak serta merta langsung dapat digunakan untuk login. Login baru dapat dilakukan setelah data diverifikasi dan dikonfirmasi oleh administratur sistem. <strong>Terima kasih.</strong></p>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
        <h4>REGISTRASI USER TBASSISTANT</h4>
        <form action="<?=BASEURL;?>Home/ctbconfirm" method="post" class="form-horizontal">
            
            <div class="form-group row">
                <label for="program" class="col-md-4">Program</label>
                <div class="col-md-8">
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="program" id="programAksara" value="KEAKSARAAN">
                        <label class="form-check-label" for="programAksara">KEAKSARAAN</label>
                    </div>

                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="program" id="programSetara" value="KESETARAAN" checked>
                        <label class="form-check-label" for="programSetara">KESETARAAN</label>
                    </div>

                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="program" id="programKursus" value="KURSUS">
                        <label class="form-check-label" for="programKursus">KETERAMPILAN FUNGSIONAL</label>
                    </div>

                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="program" id="programPaud" value="PAUD">
                        <label class="form-check-label" for="programPaud">PAUD</label>
                    </div>
                </div>
            </div>

            <div class="form-group row">
                <label for="namaPenerima" class="col-md-4">Nama Calon User</label>
                <div class="col-md-8">
                    <input type="text" name="namaPenerima" id="namaPenerima" class="form-control">
                </div>
            </div>
            
            <div class="form-group row">
                <label for="kodeNPWP" class="col-md-4">kodeNPWP</label>
                <div class="col-md-8">
                    <input type="text" name="kodeNPWP" id="kodeNPWP" class="form-control">
                </div>
            </div>
            
            <div class="form-group row">
                <label for="kota" class="col-md-4">Kota Asal SKB</label>
                <div class="col-md-8">
                    <input type="text" name="kota" id="kota" class="form-control">
                </div>
            </div>
            
            <div class="form-group row">
                <label for="namaBank" class="col-md-4">Nama BANK</label>
                <div class="col-md-8">
                    <input type="text" name="namaBank" id="namaBank" class="form-control" placeHolder="BRI Cabang / BRI Unit .... ">
                </div>
            </div>
            
            <div class="form-group row">
                <label for="tabungan" class="col-md-4">Jenis Tabungan</label>
                <div class="col-md-8">
                    <select name="tabungan" id="tabungan" class="form-control">
                        <option value="Simpedes">Simpedes</option>
                        <option value="Britama">Britama</option>
                        <option value="Tabanas">Tabanas</option>
                    </select>
                </div>
            </div>
<!-- 
    namaPenerima , kodeNPWP , namaBank , nomorRekening , namaRekTab , kota , ktb , tbUsername , tbPassword
 -->
            
            <div class="form-group row">
                <label for="nomorRekening" class="col-md-4">Nomor Rekening</label>
                <div class="col-md-8">
                    <input type="text" name="nomorRekening" id="nomorRekening" class="form-control" placeholder="xxxx-xx-xxxxxx-xx-x">
                </div>
            </div>

            <div class="form-group row">
                <label for="namaRekTab" class="col-md-4">Atas Nama</label>
                <div class="col-md-8">
                    <input type="text" name="namaRekTab" id="namaRekTab" class="form-control" placeHolder="Nama Sesuai Buku Tabungan">
                </div>
            </div>
            
            <div class="form-group row">
                <label for="submit" class="col-md-4">Periksa Data!</label>
                <div class="col-md-8">
                    <input type="submit" value="Daftar!" class="btn btn-secondary float-right">
                </div>
            </div>
            
            

        </form>
        </div>
    </div>
</div>