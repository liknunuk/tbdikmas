<?php
if($data['tb'] == NULL){
    $msg = "Data tidak ditemukan";
    $ntb = "";
    $ptb = "";
    $ktb = "";
    $usr = "";
    $psw = "";
    $utb = "";
    $sub = "<input type='submit' disabled class='btn btn-primary' value='SET LOGIN'>";
}else{
    $msg = "Data ditemukan";
    $ntb = $data['tb']['namaPenerima'];
    $ptb = $data['tb']['program'];
    $ktb = $data['tb']['kota'];
    $utb = $data['tb']['ktb'];
    $usr = $data['uname'];
    $psw = $data['upass'];
    $sub = "<input type='submit' class='btn btn-primary' value='SET LOGIN'>";
}
?>

<div class="container">
    <div class="row mt-3 d-flex justify-content-center">
        <div class="col-md-4">
            <div class="card">
                <div class="card-header">
                    <h4><?=$msg;?></h4>
                </div>
                <div class="card-body">
                    
                    <table class="table table-sm">
                        <tbody>
                            <tr>
                                <td>Nomor TB</td>
                                <td><?=$utb?></td>
                            </tr>
                            <tr>
                                <td>Nama TB</td>
                                <td><?=$ntb;?></td>
                            </tr>
                            <tr>
                                <td>SKB Kota</td>
                                <td><?=$ktb;?></td>
                            </tr>
                            <tr>
                                <td>Program</td>
                                <td><?=$ptb;?></td>
                            </tr>
                            <tr>
                                <td>Username</td>
                                <td><?=$usr;?></td>
                            </tr>
                            <tr>
                                <td>Password</td>
                                <td><?=$psw;?></td>
                            </tr>
                            <form action="<?=BASEURL;?>Home/setCred" method="POST">
                                <input type="hidden" name="ktb" value="<?=$utb;?>">
                                <input type="hidden" name="usr" value="<?=$usr;?>">
                                <input type="hidden" name="psw" value="<?=$psw;?>">
                                <tr>
                                    <td>Email</td>
                                    <td><input type="email" name="email" id="email" class="form-control" required></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td align="center">
                                        <?=$sub;?>
                                    </td>
                                </tr>
                            </form>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $this->view('template/bs4js'); ?>