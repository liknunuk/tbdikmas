<div id="login-box-wrapper">
    <img src="<?= BASEURL; ?>img/ppPaud-login.png" alt="">
    <div id="login-box-header">
        <h3>TUTOR BANTU</h3>
        <h5>PP PAUD DIKMAS JATENG</h5>
    </div>
    <div id="login-form">
        <form action="<?= BASEURL; ?>Home/login" method="post">
            <!-- <form action="#" method="post"> -->
            <div class="form-group">
                <input type="text" name="username" id="username" class="form-control" placeholder="Username">
            </div>

            <div class="form-group">
                <input type="password" name="password" id="password" class="form-control" placeholder="Password">
                <input type="checkbox" value="1" id="shopa">&nbsp;lihat password
            </div>

            <div class="form-group">
                <input type="submit" value="Login" class='btn btn-primary form-control'>
            </div>
            <!-- <div class="form-group"> -->
            <!-- <a href="<?= BASEURL; ?>Home/kelalen" class="btn btn-danger form-control">Minta Username dan Password</a> -->
            <!-- via gform -->
            <!-- https://forms.gle/SipmfsqBKAL1xYCr7 -->
            <!-- </div> -->

        </form>
    </div>
</div>
<div id="sourceLinks" class='d-flex justify-content-center py-2 bg-danger text-light'>
    <ul class="list-inline">
        <li class="list-inline-item">
            <a class='text-light' href="<?= BASEURL; ?>Home/loginPermit">Ganti Login</a>
        </li>
        <li class="list-inline-item">
            <a class='text-light' href="<?= BASEURL; ?>panduantbAssistant-tb.pdf">Panduan Tutor</a>
        </li>
        <li class="list-inline-item">
            <a class='text-light' href="https://youtu.be/FfhOTleEuK0">Video Panduan Tutor</a>
        </li>
        <li class="list-inline-item">
            <a class='text-light' href="https://youtu.be/tFLkq-OLKTU">Video Panduan Laporan</a>
        </li>
        <li class="list-inline-item">
            <a class='text-light' href="https://youtu.be/D8g7s9NDpxw">Video Panduan Ganti Login</a>
        </li>
    </ul>
</div>

<?php $this->view('template/bs4js'); ?>
<script>
    $('#shopa').change(function() {
        if (this.checked == true) {
            $("#password").prop('type', 'text');
        } else {
            $("#password").prop('type', 'password');
        }
    })
</script>