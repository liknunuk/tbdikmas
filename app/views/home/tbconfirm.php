 <!DOCTYPE html>
 <html lang="en">
 <head>
     <meta charset="UTF-8">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <title>Konfirmasi</title>
 </head>
 <body>
     <table width=600 align='center' border='1'>
        <tbody>
            <tr>
                <td>Nama User</td>
                <td><?=$data['ctb']['namaPenerima'];?></td>
            </tr>

            <tr>
                <td>Program</td>
                <td><?=$data['ctb']['program'];?></td>
            </tr>

            <tr>
                <td>Kota</td>
                <td><?=$data['ctb']['kota'];?></td>
            </tr>

            <tr>
                <td>Tabungan</td>
                <td><?=$data['ctb']['tabungan'];?> <?=substr($data['ctb']['nomorRekening'],0,4);?> <?=$data['ctb']['namaBank'];?></td>
            </tr>

            <tr>
                <td>Nomor Rekening</td>
                <td><?=$data['ctb']['nomorRekening'];?></td>
            </tr>

            <tr>
                <td>Atas Nama</td>
                <td><?=$data['ctb']['namaRekTab'];?></td>
            </tr>

            <tr style="background-color:#DDD;">
                <td>Nomor User</td>
                <td><?=$data['ktb'];?></td>
            </tr>

            <tr style="background-color:#DDD;">
                <td>Username</td>
                <td><?=$data['uname'];?></td>
            </tr>

            <tr style="background-color:#DDD;">
                <td>Password</td>
                <td><?=$data['upass'];?></td>
            </tr>

            <tr>
                <td colspan=2>
                    <form action="<?=BASEURL;?>Home/setUser" method="post">
                        <!-- ktb , program , namaPenerima , kodeNPWP , namaBank , nomorRekening , namaRekTab , kota , tbUsername , tbPassword -->
                        <input type="hidden" name="ktb" value="<?=$data['ktb'];?>">
                        <input type="hidden" name="program" value="<?=$data['ctb']['program'];?>">
                        <input type="hidden" name="namaPenerima" value="<?=$data['ctb']['namaPenerima'];?>">
                        <input type="hidden" name="kodeNPWP" value="<?=$data['ctb']['kodeNPWP'];?>">
                        <input type="hidden" name="namaBank" value="<?=$data['ctb']['tabungan'];?> <?=substr($data['ctb']['nomorRekening'],0,4);?> <?=$data['ctb']['namaBank'];?>">
                        <input type="hidden" name="nomorRekening" value="<?=$data['ctb']['nomorRekening'];?>">
                        <input type="hidden" name="namaRekTab" value="<?=$data['ctb']['namaRekTab'];?>">
                        <input type="hidden" name="kota" value="<?=$data['ctb']['kota'];?>">
                        <input type="hidden" name="tbUsername" value="<?=$data['uname'];?>">
                        <input type="hidden" name="tbPassword" value="<?=$data['upass'];?>">
                        <input type="submit" value="Daftar !">
                    </form>
                </td>
            </tr>
            
        </tbody>
     </table>

 </body>
 </html>