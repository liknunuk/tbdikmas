<div id="login-box-wrapper">
    <img src="<?=BASEURL;?>img/ppPaud-login.png" alt="">
    <div id="login-box-header">
        <h3>TUTOR BANTU</h3>
        <h5>PP PAUD DIKMAS JATENG</h5>
    </div>
    <div id="login-form" style=" margin-top: 0px;">
        <form action="<?=BASEURL;?>Home/uvrf" method="post">
            <div class="form-group">
                <label for="kodeNPWP">Kode NPWP</label>
                <input type="text" name="kodeNPWP" id="kodeNPWP" class="form-control" placeholder="xx.xxx.xxx.x-xxx.xxx">
            </div>

            <div class="form-group">
                <label for="nomorRekening">No. Rekening BRI</label>
                <input type="nomorRekening" name="nomorRekening" id="nomorRekening" class="form-control" placeholder="xxxx-xx-xxxxxx-xx-x">
            </div>

            <div class="form-group">
                <input type="submit" value="Verify" class='btn btn-primary form-control'>
            </div>
        </form>
    </div>
</div>

<?php $this->view('template/bs4js'); ?>
