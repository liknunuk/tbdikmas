<div class="container-fluid">
  <!-- Daftar Kota TB -->
  <div class="row">
    <div class="col-md-4 mx-auto mt-3">
        <div class="form-group">
            <label for="kotatb">Kota Asal SKB Tutor</label>
            <select id="kotatb" class="form-control">
                <option value="">Pilih Kota</option>
                <?php foreach($data['kota'] as $kota ): ?>
                <option value="<?=$kota['kota'];?>"><?=$kota['kota'];?></option>
                <?php endforeach; ?>
            </select>
        </div>
    </div>    
  </div>

  <!-- Daftar TB per Kota-->
  <div class="row">
    <div class="col-md-4 mx-auto">
        <table class="table table-sm">
            <tbody id="lotutor">
            </tbody>
        </table>
    </div>    
  </div>

  <!-- formulir set login-->
  <div class="row" id="formLali" style="display:none;">
    <div class="col-md-4 mx-auto">
      <form action="<?=BASEURL;?>Home/bosen" method="post">
        
        <div class="form-group">
          <label for="ktb">Kode TB</label>
          <input type="text" name="ktb" id="ktb" class="form-control" readonly>
        </div>

        <div class="form-group">
          <label for="username">email</label>
          <input type="email" name="email" id="email" class="form-control" required>
        </div>

        <div class="form-group">
          <label for="password">Pasword Baru</label>
          <input type="password" name="password" id="password" class="form-control" required>
        </div>

        <div class="form-group">
           <input type="submit" value="Ganti Password" class="btn btn-success form-control">
        </div>
      </form>
    </div>    
  </div>
</div>

<?php $this->view('template/bs4js'); ?>
<script>
$("#kotatb").on('change',function(){
    $.ajax({
        method:'POST',
        dataType : 'json',
        url: '<?=BASEURL;?>Home/tutore',
        data: {kota: this.value },
        success: function(resp){
            console.log(resp);
            $("#lotutor tr").remove();
            $.each(resp , function(i,data){
                $('#lotutor').append(`
                <tr>
                <td>${data.ktb}</td>
                <td><a href='javascript:void(0)' class='tutore' id='${data.ktb}'>${data.namaPenerima}</a></td>
                </tr>
                `)
            })
        }
    })
})

$('#lotutor').on('click','.tutore',function(){
    $("#formLali").show();
    $("#ktb").val( this.id );
})
</script>