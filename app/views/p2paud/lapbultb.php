<div class="container-fluid">
    <div class="row page-title">
        <div class="col-lg-12">
       <h3>LAPORAN BULANAN TUTOR BANTU</h3>
        </div>
    </div>

    <?php
        $thn = date('Y');
        $abu = [
            '01'=>'Januari',
            '02'=>'Februari',
            '03'=>'Maret',
            '04'=>'April',
            '05'=>'Mei',
            '06'=>'Juni',
            '07'=>'Juli',
            '08'=>'Agustus',
            '09'=>'September',
            '10'=>'Oktober',
            '11'=>'Nopember',
            '12'=>'Desember'
        ];

        $blb = substr($data['bulan'],-2);
    ?>

    <div class="row">
        <div class="col-lg-2">
            <h4>Bulan:</h4>
        </div>
        <div class="col-lg-2">
            <div class="form-group">
                <select id="bulap" class="form-control">
                    <?php 
                        foreach($abu as $ab=>$nb): 
                        $sel = $data['bulan'] == $thn.'-'.$ab ? 'Selected' : '';
                    ?>
                    <option value="<?=$thn.'-'.$ab;?>" <?=$sel?> >
                    <?=$nb.' '.$thn;?>
                    </option>
                    <?php endforeach; ?>
                </select>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 table-responsive">
        <?php Alert::sankil(); ?>
            <table class="table table-sm table-bordered table-striped">
                <thead>
                    <tr>
                        <th>Bulan</th>
                        <th>SKB Kota</th>
                        <th>Nama Tutor Bantu</th>
                        <th>URL Laporan</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($data['labul'] AS $labul): ?>
                        <tr>
                            <td><?=$labul['bulan'];?></td>
                            <td><?=$labul['kota'];?></td>
                            <td>[<?=$labul['ktb'];?>] - <?=$labul['namaPenerima'];?></td>
                            <td>
                            <?php
                                $txtLap = explode("/",$labul['dgLink']);
                                echo "<a href='".$labul['dgLink']."' target='_blank'>".$txtLap[5]."</a>";
                            ?>
                            </td>
                            <!-- <td><?=$labul['dgLink'];?></td> -->
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<?php $this->view('template/bs4js'); ?>
<script>
    $('#bulap').change( function(){
        window.location="<?=BASEURL;?>P2paud/tblabul/"+this.value;
    })
</script>