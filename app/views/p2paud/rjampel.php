<div class="container-fluid mt-3">
    <div class="row">
        <div class="col-lg-9 text-center">
            <h2>REKAP JAM PELAJARAN TUTOR BANTU</h2>
        </div>
        <div class="col-lg-3 form-group row">
            <label for="bulan" class="col-md-4">Bulan</label>
            <div class="col-md-8">
                <select id="bulan" class="form-control">
                    <?php
                        $tahun = date('Y');
                        $bulan = [
                            "01"=>"Januari",
                            "02"=>"Februari",
                            "03"=>"Maret",
                            "04"=>"April",
                            "05"=>"Mei",
                            "06"=>"Juni",
                            "07"=>"Juli",
                            "08"=>"Agustus",
                            "09"=>"September",
                            "10"=>"Oktober",
                            "11"=>"Nopember",
                            "12"=>"Desember"
                        ];

                        foreach($bulan AS $ank=>$nmb):
                            if ("$tahun-$ank" == $data['bulan']){ $sel = "selected" ; }else{ $sel = ""; }
                    ?>

                            <option <?=$sel;?> value="<?=$tahun."-".$ank;?>"><?=$nmb." ".$tahun;?></option>

                            <?php endforeach; ?>
                </select>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Nomor Urut</th>
                        <th>Nama Tutor Bantu</th>
                        <th>Program</th>
                        <th>SKB Kota/Kab</th>
                        <th>Bulan</th>
                        <th>Jam Pelajaran</th>
                    </tr>
                </thead>
                <tbody>
                <?php foreach($data['rekjp'] AS $jp ): ?>
                    <tr>
                        <td><?=$jp['ktb'];?></td>
                        <td><?=$jp['namaPenerima'];?></td>
                        <td><?=$jp['program'];?></td>
                        <td><?=$jp['kota'];?></td>
                        <td><?=$jp['bulan'];?></td>
                        <td class="text-right"><?=$jp['jampel'];?> Jam</td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<?php $this->view('template/bs4js'); ?>
<script>
    $("#bulan").change( function(){
        window.location="<?=BASEURL;?>Jp/rekap/"+this.value;
    })
</script>