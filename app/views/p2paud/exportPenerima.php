<?php
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=PenerimaTransfer-".$data['trf']['program']."-".$data['trf']['bulan'].".xls");
?>
<table class="table table-sm">
    <thead>
        <tr>
            <?php foreach($data['kolomTabel'] as $kolom): ?>
            <th><?=$kolom;?></th>
            <?php endforeach; ?>
        </tr>
    </thead>
    <tbody>
        <?php
            $no = 1;
            foreach($data['penerima'] AS $tutor):
        ?>
        <tr>
            <td><?=$tutor['ktb'];?></td>
            <td><?=$tutor['namaPenerima'];?></td>
            <td><?=$tutor['kodeNPWP'];?></td>
            <td><?=$tutor['namaRekTab'];?></td>
            <td><?=$tutor['namaBank'];?></td>
            <td><?=$tutor['nomorRekening'];?></td>
            <td class='text-right'><?=number_format($tutor['nilaiRupiah'],0,',','.');?></td>
            <td><?=$tutor['kodeBank'];?></td>
            <td><?=$tutor['kodeBankSpan'];?></td>
            <td><?=$tutor['kodeSwift'];?></td>
            <td><?=$tutor['kodePos'];?></td>
            <td><?=$tutor['kodeNegara'];?></td>
            <td><?=$tutor['kodeKppn'];?></td>
            <td><?=$tutor['tipeSuplier'];?></td>
            <td><?=$tutor['kota'];?></td>
            <td><?=$tutor['provinsi'];?></td>
            <td><?=$tutor['email'];?></td>
            <td><?=$tutor['telepon'];?></td>
            <td><?=$tutor['kodeIban'];?></td>
            <td><?=$tutor['alamatPenerima'];?></td>
            <td><?=$tutor['kodeNegaraBank'];?></td>
            <td><?=$tutor['kodePropinsi'];?></td>
            <td><?=$tutor['kodeKabKota'];?></td>
            <td><?=$tutor['nrs'];?></td>
            <td><?=$tutor['nip'];?></td>
        </tr>
        <?php

            $no++;
            endforeach;
        ?>
    </tbody>
</table>