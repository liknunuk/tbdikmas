<div class="container-fluid">
    <div class="row page-title">
        <div class="col-lg-12"><h3>FORMULIR DATA TUTOR BANTU JAWA TENGAH</h3></div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <form action="<?=BASEURL;?>P2paud/setTutor" method="POST" class="form-horizontal">
                
                <input type="hidden" name="mod" id="mod" value="<?=$data['mod'];?>">
                <input type="hidden" name="nomor" id="nomor" value="<?=$data['tutor']['nomor'];?>">
                <div class="form-group row">
                    <label for="program" class="col-lg-3">Program</label>
                    <div class="col-lg-9">
                        <select name="program" id="program" class="form-control" value="<?=$data['tutor']['program'];?>" class="form-control">
                        <option value="KEAKSARAAN">KEAKSARAAN</option>
                        <option value="KESETARAAN">KESETARAAN</option>
                        <option value="KURSUS">KURSUS</option>
                        <option value="PAUD">PAUD</option>
                        </select>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="namaPenerima" class="col-lg-3">Nama Tutor Bantu</label>
                    <div class="col-lg-9">
                        <input type="text" name="namaPenerima" id="namaPenerima" class="form-control" value="<?=$data['tutor']['namaPenerima'];?>" class="form-control">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="kodeNPWP" class="col-lg-3">N P W P</label>
                    <div class="col-lg-9">
                        <input type="text" name="kodeNPWP" id="kodeNPWP" class="form-control" value="<?=$data['tutor']['kodeNPWP'];?>" class="form-control">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="Kota" class="col-lg-3">Kota Asal SKB</label>
                    <div class="col-lg-9">
                        <input type="text" name="Kota" id="Kota" class="form-control" value="<?=$data['tutor']['Kota'];?>" class="form-control">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="namaRekTab" class="col-lg-3">Nama dalam Rekening Tabungan</label>
                    <div class="col-lg-9">
                        <input type="text" name="namaRekTab" id="namaRekTab" class="form-control" value="<?=$data['tutor']['namaRekTab'];?>" class="form-control">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="namaBank" class="col-lg-3">Nama Bank</label>
                    <div class="col-lg-9">
                        <input type="text" name="namaBank" id="namaBank" class="form-control" value="<?=$data['tutor']['namaBank'];?>" class="form-control">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="nomorRekening" class="col-lg-3">Nomor Rekening Tabungan</label>
                    <div class="col-lg-9">
                        <input type="text" name="nomorRekening" id="nomorRekening" class="form-control" value="<?=$data['tutor']['nomorRekening'];?>" class="form-control">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="nilaiRupiah" class="col-lg-3">Nilai Transfer (Rp)</label>
                    <div class="col-lg-9">
                        <input type="number" name="nilaiRupiah" id="nilaiRupiah" min="0" class="form-control" value="<?=$data['tutor']['nilaiRupiah'];?>" class="form-control">
                    </div>
                </div>

                <?php if($data['mod'] == 'enggal'): ?>

                <div class="form-group row">
                    <label for="tbUsername" class="col-lg-3">Username</label>
                    <div class="col-lg-9">
                        <input type="text" name="tbUsername" id="tbUsername" min="0" class="form-control" value="<?=$data['cred']['username'];?>" class="form-control">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="tbPassword" class="col-lg-3">Password</label>
                    <div class="col-lg-9">
                        <input type="text" name="tbPassword" id="tbPassword" min="0" class="form-control" value="<?=$data['cred']['password'];?>" class="form-control">
                    </div>
                </div>

                <?php endif; ?>

                <div class="form-group row">
                    <label for="cekData" class="col-lg-3">Cek Data</label>
                    <div class="col-lg-9">
                        <input type="submit" value="Simpan" class="btn btn-primary" id="cekData">
                    </div>
                </div>

            </form>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12"></div>
    </div>
</div>

<?php $this->view('template/bs4js'); ?>
