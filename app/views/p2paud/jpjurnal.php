<?php

function jampelmu($duration)
{
    list($j, $m, $d) = explode(":", $duration);
    $mj = (int)$j * 60;
    $mm = (int)$m * 1;
    $jp = ($mj + $mm) / 45;
    return $jp;
}

function jampele($r, $l, $j)
{
    $f = makeIntTime($r);
    $s = makeIntTime($l);
    $sejampel = $j * 60;
    return floor(($f - $s) / $sejampel);
}

function makeIntTime($t)
{
    list($h, $m, $s) = explode(":", $t);
    $timeStample = mktime($h, $m);
    return $timeStample;
}

?>
<div class="container-fluid">
    <div class="row page-title">
        <div class="col-lg-12">
            <h1>Jampel Menurut Jurnal</h1>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">

            <ul class="list-group list-group-horizontal">
                <?php foreach ($data['majurn'] as $maj) : ?>
                    <li class="list-group-item"><a href="<?= BASEURL; ?>P2paud/tbJampel/<?= $data['tbid']; ?>/<?= $maj['bulan']; ?>"><?= $maj['bulan']; ?></a></li>
                <?php endforeach; ?>
            </ul>
            <table class="table table-sm table-striped">
                <thead>
                    <tr>
                        <th>No. Jurnal</th>
                        <th>Kelas</th>
                        <th>Kegiatan</th>
                        <th>Durasi</th>
                        <th>Jampel</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $tjp = 0;
                    foreach ($data['jampel'] as $jpk) :
                    ?>
                        <tr>
                            <td><?= $jpk['idxJurnal']; ?></td>
                            <td><?= $jpk['sessionName']; ?></td>
                            <td><?= $jpk['kegiatan']; ?></td>
                            <td><?= $jpk['tanggal']; ?> , <?= $jpk['jamMulai']; ?> s.d <?= $jpk['jamAkhir']; ?></td>
                            <td class='text-right'>
                                <?php
                                $jp = jampele($jpk['jamAkhir'], $jpk['jamMulai'], $jpk['mjp']);
                                echo $jp . ' jp @ ' . $jpk['mjp'];
                                ?>
                            </td>
                        </tr>
                    <?php
                        $tjp += $jp;
                    endforeach;
                    ?>
                    <tr>
                        <td colspan='4' class='text-right'>Total Jampel Bulanan</td>
                        <td class='text-right'><?= $tjp; ?> jp</td>
                    </tr>
                </tbody>
            </table>
        </div>

    </div>
</div>