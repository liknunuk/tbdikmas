<div class="container-fluid">
    <div class="row page-title">
        <div class="col-lg-12">
            <h3>Data Transfer Honorarium Tutor Bantu</h3>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6">
            <table class="table table-sm">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Waktu</th>
                        <th>Bulan</th>
                        <th>Program</th>
                        <th>Kontrol</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($data['transfer'] as $trf) : ?>
                        <tr>
                            <td><?= $trf['idxTrf']; ?></td>
                            <td><?= $trf['waktu']; ?></td>
                            <td><?= $trf['bulan']; ?></td>
                            <td><?= $trf['program']; ?></td>
                            <td>
                                <a href="javascript:void(0)" class="penerima" id="<?= $trf['idxTrf']; ?>">Penerima</a> |
                                <a href="javascript:void(0)" class="export" id="trx-<?= $trf['idxTrf']; ?>">Export</a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
        <div class="col-lg-2">
            <div class="form-group">
                <label for="bulan">Bulan</label>
                <?php
                $tahun = date('Y');
                $bulan = [
                    '01' => 'Januari',
                    '02' => 'Februari',
                    '03' => 'Maret',
                    '04' => 'April',
                    '05' => 'Mei',
                    '06' => 'Juni',
                    '07' => 'Juli',
                    '08' => 'Agustus',
                    '09' => 'September',
                    '10' => 'Oktober',
                    '11' => 'Nopember',
                    '12' => 'Desember'
                ];
                echo "<select class='form-control' id='bulan'>";
                foreach ($bulan as $angka => $nama) {
                    if ("$tahun-$angka" == date('Y-m')) {
                        $sel = "selected";
                    } else {
                        $sel = "";
                    }

                    echo "<option $sel value='$tahun-$angka'>$nama $tahun</option>";
                }
                echo "</select>";
                ?>
            </div>
        </div>
        <div class="col-lg-2">
            <div class="form-group">
                <label for="program">Program</label>
                <select id="program" class="form-control">
                    <option value="TP">TUTOR PAUD</option>
                    <option value="TS">TUTOR SETARA</option>
                </select>
            </div>
        </div>
        <div class="col-lg-2">
            <label>&nbsp;</label>
            <button class="form-control btn btn-primary" id="datatransfer">Data Transfer</button>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12" id="tutors">
            <h5 class='font-weight-bold py-2 px-3'>Data Transfer Bulan : <?= $data['bulan']; ?>. Program: <?= $data['program']; ?></h5>
            <table class="table table-sm table-bordered">
                <thead>
                    <tr>
                        <?php foreach ($data['kolomTabel'] as $kolom) : ?>
                            <th><?= $kolom; ?></th>
                        <?php endforeach; ?>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>

<?php $this->view('template/bs4js'); ?>
<script>
    $('.penerima').click(function() {
        let idtrf = this.id;
        $('#tutors table').remove();
        $.ajax({
            url: '<?= BASEURL; ?>P2paud/penerimaBulanan/' + idtrf,
            success: function(penerima) {
                $("#tutors").html(penerima);
            }
        });
    })

    $('.export').click(function() {
        let idfr = this.id.split('-');
        let url = '<?= BASEURL; ?>P2paud/exportPenerima/' + idfr[1];
        window.open(url, 'export', 'width=600,height=200,left=200,top=200');
    })

    $("#datatransfer").click(function() {
        let bl = $('#bulan').val(),
            pg = $('#program').val();
        window.location = "<?= BASEURL; ?>P2paud/buktiTransfer/" + bl + "/" + pg;
    })
</script>