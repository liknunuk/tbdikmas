<div class="container-fluid">
    <div class="row page-title">
        <div class="col-lg-12">
       <h3>DAFTAR TUTOR BANTU</h3>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-2">
            <a href="<?=BASEURL;?>P2paud/tbform/enggal" class="btn btn-primary">Tambah Tutor</a>
        </div>
        <div class="col-lg-5 form-group">
            <input type="text" id="namaTutor" class="form-control" placeHolder="Cari Nama Tutor">
        </div>
        <div class="col-lg-5">
            <?=$data['pagina']; ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 table-responsive">
        <?php Alert::sankil(); ?>
            <table class="table table-sm table-bordered table-striped">
                <thead>
                    <tr>
                        <?php foreach($data['kolom'] AS $kolom): ;?>
                        <th><?=$kolom?></th>
                        <?php endforeach; ?>
                        <th>Kontrol</th>
                    </tr>
                </thead>
                <tbody id="tutors">
                    <?php foreach($data['tutors'] AS $tutor): ?>
                        <tr>
                            <td><?=$tutor['ktb'];?><br/><small><i><?=$tutor['program'];?></i></small></td>
                            <td><?=$tutor['namaPenerima'];?><br><span class="rektab">(<?=$tutor['namaRekTab'];?>)</span></td>
                            <td><?=$tutor['kodeNPWP'];?></td>
                            <td><?=$tutor['namaBank'];?></td>
                            <td><?=$tutor['nomorRekening'];?></td>
                            <td class='text-right'><?=number_format($tutor['nilaiRupiah'],0,',','.');?></td>
                            <td><?=$tutor['kota'];?></td>
                            <td>
                                <a href="<?=BASEURL;?>P2paud/tbform/gantos/<?=$tutor['nomor'];?>"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                <a href="javascript:void(0)" class="tbcuthat" id="tbc_<?=$tutor['nomor'];?>"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                <a href="<?=BASEURL;?>P2paud/tbJampel/<?=sprintf('%03d',$tutor['nomor']);?>"><i class="fa fa-clock-o"></i></a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
    <?=$data['pagina']; ?>
</div>
<?php $this->view('template/bs4js'); ?>
<script>
    $('.tbcuthat').click(function(){
        let tbid = this.id.split("_");
        let tenane = confirm('Data akan dihapus!');
        if( tenane == true ){
            $.post('<?=BASEURL;?>P2paud/tbfired',{
                tbid : tbid[1]
            },function(resp){
                if(resp=='1'){location.reload();}
            })
        }
    })

    $('#namaTutor').keyup(function(){
        let tutor = this.value;
        if(tutor.length >= 5){
            $.ajax({
                dataType: 'json',
                url: '<?=BASEURL;?>P2paud/srcTutorRslt/'+tutor,
                success: function(resp){
                    $("#tutors tr").remove();
                    $.each( resp , function(i,data){
                        $("#tutors").append(`
                        <tr>
                            <td>${data.ktb}<br/><small><i>{data.program}</i></small></td>
                            <td>${data.namaPenerima}<br><span class="rektab">(${data.namaRekTab})</span></td>
                            <td>${data.kodeNPWP}</td>
                            <td>${data.namaBank}</td>
                            <td>${data.nomorRekening}</td>
                            <td class='text-right'>` + parseInt(data.nilaiRupiah).toLocaleString('id-ID') +`</td>
                            <td>${data.kota}</td>
                            <td>
                                <a href="<?=BASEURL;?>P2paud/tbform/gantos/${data.nomor}">Edit</a>
                                <a href="javascript:void(0)" class="tbcuthat" id="tbc_${data.nomor}">Hapus</a>
                            </td>
                        </tr>
                        
                        `);
                    })
                }
            })
        }else if( tutor.length == 0 ){
            location.reload();
        }
    })
</script>
