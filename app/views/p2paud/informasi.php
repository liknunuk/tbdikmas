<div class="container-fluid">
    <div class="row page-title">
        <div class="col-lg-12">
            <h3>INFORMASI TUTOR BANTU</h3>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <?php Alert::sankil(); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6">
            <form action="<?=BASEURL;?>P2paud/setInfo" method="post">
            <!-- author,waktu,judul,berita -->
                <input type="hidden" name="author" value="P2PAUDDIKMAS JATENG">
                <input type="hidden" name="waktu" value="<?=$data['kabar']['waktu'];?>">
                <input type="hidden" name="mod" value="<?=$data['mod'];?>">
                <input type="hidden" name="idxInfo" value="<?=$data['kabar']['idxInfo'];?>">
                <div class="form-group">
                    <label for="judul">Judul Berita</label>
                    <input type="text" name="judul" id="judul" class="form-control" value="<?=$data['kabar']['judul'];?>">
                </div>
                <div class="form-group">
                    <label for="berita">Isi Berita</label>
                    <textarea name="berita" id="berita" rows="10" class="form-control artikel"><?=$data['kabar']['berita'];?></textarea>
                </div>
                <div class="form-group d-flex justify-content-end">
                    <input type="submit" value="Simpan" class="btn btn-success">
                </div>
            </form>
        </div>
        <div class="col-lg-6">
            <?php
            foreach($data['berita'] as $berita): ?>
                 <!-- $berita['idxInfo'],$berita['author'],$berita['waktu'],$berita['judul'],$berita['berita'],  -->
                 <article>
                    <span><small><em><?=$berita['author'];?> - <?=$berita['waktu'];?></em></small></span>
                    <h4><?=$berita['judul'];?></h4>
                    <?php 
                        $txtBerita = explode(" ",$berita['berita']);
                        $isiBerita = "";
                        for($i = 0 ; $i < 30 ; $i++){
                            $isiBerita.=$txtBerita[$i]." ";
                            if($i == count($txtBerita) - 2){
                                break;
                            }
                        }
                    ?>
                    <p><?=$isiBerita;?> ... 
                    <small>
                        <em>
                            <a href="<?=BASEURL;?>P2paud/tbinfo/<?=$berita['idxInfo'];?>">Selengkapnya</a>
                        </em>
                    </small> </p>
                 </article>
            <?php endforeach; ?>
        </div>
    </div>
</div>

<?php 
$this->view('template/bs4js');
$this->view('template/tinymce');
?>