<div class="container-fluid">
    <div class="row page-title">
        <div class="col-lg-12">
            <h3>PENCAIRAN HONORARIUM TUTOR BANTU</h3>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-3 form-horizontal">
            <div class="form-group row">
                <label for="bulan" class="col-sm-6">Bulan</label>
                <div class="col-sm-6">
                    <?php //echo $data['bulan'] . ' - ' . date('Y-m'); 
                    ?>
                    <select id="bulan" class="form-control">
                        <?php
                        $tahun = date('Y');
                        foreach (range(1, 12) as $bulan) {
                            if ($data['bulan'] == $tahun . '-' . sprintf("%02d", $bulan)) {
                                $selected = "selected";
                            } else {
                                $selected = "";
                            }
                            echo "<option value='" . $tahun . "-" . sprintf("%02d", $bulan) . "' $selected >" . sprintf("%02d", $bulan) . "-" . $tahun . "</option>";
                        }
                        ?>
                    </select>
                </div>
            </div>
        </div>
        <div class="col-lg-9">
            <ul class="list-inline">
                <li class="list-inline-item">KLP PROGRAM: </li>
                <?php
                $programs = ['TS', 'TP'];
                foreach ($programs as $program) : ?>
                    <li class="list-inline-item w100"><a href="javascript:void(0)" class='program'><?= $program; ?></a></li>
                <?php endforeach; ?>
            </ul>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-10">
            <div>
                <h4>Klp. Program: <span id="program"><?= $data['program']; ?></span></h4>
            </div>
        </div>
        <div class="col-lg-2">
            <div class="form-check">
                <input class="form-check-input" type="checkbox" id="pilihSemua">
                <label class="form-check-label" for="pilihSemua">
                    Pilih Semua
                </label>
            </div>
        </div>
    </div>
    <div class='row'>
        <?php foreach ($data['penerima'] as $penerima) : ?>
            <div class="col-md-3">
                <table class="table table-sm bg-light">
                    <tbody>
                        <tr>
                            <td width="20" class='align-middle'>
                                <input type="checkbox" class="penerima" id="<?= $penerima['nomor']; ?>">
                            </td>
                            <td>
                                <span class="kota"><?= $penerima['kota']; ?></span><br />
                                <span class="nama"><?= $penerima['namaPenerima']; ?></span>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        <?php endforeach; ?>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <button id="setTransfer" class="btn btn-primary float-right mx-3">Proses</button>
        </div>
    </div>
</div>


<?php $this->view('template/bs4js'); ?>
<script>
    $('.program').click(function() {
        window.location = '<?= BASEURL; ?>P2paud/transfer/' + $(this).text();
    })

    $('#pilihSemua').change(function() {
        let checked = $(this).prop('checked');
        if (checked == true) {
            $('.penerima').prop('checked', true);
        } else {
            $('.penerima').prop('checked', false);
        }
    })

    $("#setTransfer").click(function() {
        let i = 0,
            tb = [];
        $.each($('.penerima:checked'), function() {
            tb.push(this.id);
            i += 1;
        })

        if (i == 0) {
            alert('Pilih Penerima!');
        } else {
            // post data
            let bln = $("#bulan").val(),
                pro = $("#program").text(),
                tbp = tb;
            $.post('<?= BASEURL; ?>P2paud/setTransfer', {
                bulan: bln,
                program: pro,
                penerima: tbp
            }, function(resp) {
                if (resp == '1') {
                    window.location = "<?= BASEURL; ?>P2paud/buktiTransfer/" + bln + "/" + pro;
                }
                // console.log(resp);
            })
        }
    })
</script>