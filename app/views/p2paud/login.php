<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?=$data['title'];?></title>
    <!-- bootstrap 4 CDN -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <!-- jquery-ui -->
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <!-- font awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Custom style -->
    
    <style>
        html, body {
        height: 100%;
        }
        .full-h { height: 100%;}

        #p2welcome {
            background-image: url('../img/welcome.jpg');
            background-size: 100% auto;
            background-attachment: fixed;
            background-repeat: no-repeat;
        }

        #login-wrapper{
            display: flex;
            width: 80%; height: 100%;
            margin: auto;
            justify-content: flex-end;
            align-items: center;
        }

        #p2loginbox{
            height: 400px;
            width: 320px;
            background-color: #FFFFFFAA;

        }

        #login-banner { height: 80px; width: 100%; border: 0px dashed black;}
        #login-banner > img { display: block; height: 60px; width: 60px; margin: 10px auto;}

        #login-title-box { text-align: center; border: 0px dashed black; padding: 0;}
        #login-title-box > p { font-size: 18px; margin:10px 0; font-weight: bold;}

        #login-form {
            padding: 20px;  border: 0px dashed black; margin: 0;
        }
        /* ipad - portrait */
        @media screen and ( max-width: 768px ){
    
            #p2welcome {
                background-size:auto 100%;
            }

            #login-wrapper{
                width: 100%;
                margin:0;
                justify-content: center;
            }
        }
        
        /* ipad - landscape */
        @media screen and ( max-width: 1024px ){
            
            #p2welcome {
                background-size:auto 100%;
            }
            
            #login-wrapper{
                width: 90%;
                margin-left: 5%;
            }
        }


    </style>
</head>
<body id="p2welcome">
    
    <div id="login-wrapper">
        <div id="p2loginbox">
            <div id="login-banner">
                <img src="https://awsimages.detik.net.id/community/media/visual/2019/09/05/f2399a3c-e44e-4e2a-82d0-520696ffad1b.png?w=700&q=90" alt="tutwuri">
            </div>
            <div id="login-title-box">
                <p>PP PAUD &amp; DIKMAS JATENG</p>
            </div>
            <div id="login-form">
                <form action="<?=BASEURL;?>P2paud/auth" method="post">
                    <div class="form-group">
                        <label for="username">Username</label>
                        <input type="text" name="username" id="username" class="form-control" required >
                    </div>

                    <div class="form-group">
                        <label for="password">Password</label>
                        <input type="password" name="password" id="password" class="form-control" required >
                    </div>

                    <div class="form-group">
                        <button type="submit" class="form-control btn btn-primary">Masuk</button>
                    </div>

                </form>
            </div>
        </div>
    </div>
    
</div>    
    <!-- Jquery Scripts -->

    <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

    <!-- jquery-ui script  -->
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <!-- Jquery Scripts -->
</body>
</html>